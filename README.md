# Codenames

Proyek Advanced Programming Kelas A, Kelompok 9

**Anggota:**
1.  Doan Andreas Nathanael (1806205123)
2.  Nanda Ryaas Absar (1806141391)
3.  Muzaki Azami Khairevy (1806205470)
4.  Riri Edwina Renaganis (1806186710)

[![pipeline status](https://gitlab.com/doanandreas/advprog-9-codenames/badges/gameservice-master/pipeline.svg)](https://gitlab.com/doanandreas/advprog-9-codenames/-/commits/gameservice-master)
[![coverage report](https://gitlab.com/doanandreas/advprog-9-codenames/badges/gameservice-master/coverage.svg)](https://gitlab.com/doanandreas/advprog-9-codenames/-/commits/gameservice-master)

**Deskripsi:**

Codenames adalah suatu boardgames dengan tujuan menebak kata. 
Pada projek ini kami membuat ulang dengan menggunakan chatbot.

Pemain terdiri dari 2 tim dan di setiap tim terdiri dari 2 orang (2 vs 2). 
Setiap pasangan akan diberi waktu untuk menebak kata 
dimana salah satu akan memberikan clue 
dan salah satunya akan menebak kata berdasarkan clue tersebut.

Jika penebak dapat menebak kata dengan benar, 
maka timnya akan mendapat satu poin.

Video tutorial permainan:
https://youtu.be/zQVHkl8oQEU

**Fitur:**

1.  Timer

Timer akan digunakan untuk memberikan waktu pada pemain.
Timer sendiri menunjukkan waktu yang tersisa bagi tim yang sedang bermain
dan tim yang sedang menebak.

2. Codenames

Permainannya itu sendiri adalah fitur yang kita gunakan.
Pada fitur ini terdapat fitur storage untuk
mengenerate kata – kata yang harus di
tebak serta code untuk masuk ke dalam game room.

3. Lobby

Lobby merupakan game room.
Isi dari tiap lobby adalah permainan yang sedang berlangsung.
Diharapkan kami dapat membuat banyak lobby (tidak
bergantung lobby yang satu dengan lobby yang lainnya)
agar tidak hanya satu permainan yang bisa dijalankan.

**Implementasi:**
1. Membuat Lobby

Satu pemain akan meminta chatbot untuk membuat suatu game room,
kemudian chatbot akan memberikan kode akses kepada pemain.

2. Memasuki Game Room

Pemain dengan kode akses yang sama
akan memasuki game room yang sama.

3. Permainan Dimulai

Timer akan menunjukkan waktu yang sama baik
sisa waktu bermain dan sisa waktu menunggu.

4. Penilaian

Pemain akan mencetak nilai apabila kata yang tepat berhasil ditebak. 
Pemain dengan nilai lebih banyak akan menjadi pemenang.

5. Hasil Permainan

Hasil dari games akan keluar jika seluruh ronde permainan selesai.
Jumlah ronde dapat ditentukan oleh pemain.

**Design Pattern yang Digunakan:**
1. Singleton (Timer)

Timer yang digunakan pada setiap permainan hanya 1 dan
menunjukan waktu yang sama. Dikerjakan 1 orang.

2. Observer (Codenames & Lobby)

Untuk Codenames, akan mem-broadcast jawaban kepada seluruh pemain. Dikerjakan 1-2 orang.
Untuk Lobby, akan mem-broadcast nilai akhir kepada seluruh pemain. Dikerjakan 1-2 orang.


