package com.adprog9.botcodenames.controller.commands;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import com.adprog9.botcodenames.handler.helper.StateHelper;
import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.handler.state.pregamestate.GeneralState;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.repository.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class InitialHandlerTest {
    String dummyId;
    String displayName;
    Player dummyPlayer;
    String roomId;
    String command;

    @Mock
    GeneralState generalState;

    @Mock
    StateHelper anotherStateHelper;

    @Mock
    PlayerRepository playerRepository;

    @InjectMocks
    StateHelper stateHelper = new StateHelper();

    @InjectMocks
    InitialHandler initialHandler = new InitialHandler();

    @BeforeEach
    void setUp() {
        dummyId = "dummy";
        displayName = "Codenames";
        dummyPlayer = new Player(dummyId, displayName, null, null);
        roomId = "123";
        when(playerRepository.save(dummyPlayer)).thenReturn(dummyPlayer);
        playerRepository.save(dummyPlayer);
    }

    @Test
    void handleCommandHelp() {
        command = "/help";
        String expectedOutput = Messages.HELP;
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
//        when(generalState.help(dummyId)).thenReturn(expectedOutput);
        assertEquals(dummyPlayer, playerRepository.findPlayerByUserId(dummyId));
//        when(stateHelper.getUserState(dummyId)).thenReturn(generalState);
//        String receivedOutput = initialHandler.handleCommand(command, dummyId, displayName);
//        assertEquals(expectedOutput, receivedOutput);
    }
}