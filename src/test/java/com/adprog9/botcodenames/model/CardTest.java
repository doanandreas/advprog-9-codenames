package com.adprog9.botcodenames.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CardTest {

    private Card card;

    @BeforeEach
    public void setUp() throws Exception {
        this.card = new Card("Hollywood");
    }

    @Test
    void testGetCardToBoards() {
        int expectedOutput = 0;
        int currentImplOutput = card.getCardToBoards().size();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetCardToBoards() {
        List<CardToBoard> expectedOutput = new ArrayList<CardToBoard>(
                Arrays.asList(new CardToBoard()));
        card.setCardToBoards(expectedOutput);
        List<CardToBoard> currentImplOutput = card.getCardToBoards();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetId() {
        long expectedOutput = 0L;
        long currentImplOutput = card.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetId() {
        card.setId(2L);
        long expectedOutput = 2L;
        long currentImplOutput = card.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetWord() {
        card.setWord("Dummy");
        String expectedOutput = "Dummy";
        String currentImplOutput = card.getWord();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetWord() {
        String expectedOutput = "Hollywood";
        String currentImplOutput = card.getWord();

        assertEquals(expectedOutput, currentImplOutput);
    }
}