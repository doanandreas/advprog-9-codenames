package com.adprog9.botcodenames.model;

import static org.junit.jupiter.api.Assertions.*;

import com.adprog9.botcodenames.handler.messages.Messages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PlayerTest {

    String dummyIdLine;
    String dummyDisplayName;
    String dummyTeam;
    String dummyRole;
    Player dummyPlayer;
    Room dummyRoom;

    @BeforeEach
    void setUp() {
        dummyIdLine = "dummy";
        dummyDisplayName = "Dummy";
        dummyTeam = null;
        dummyRole = null;
        dummyPlayer = new Player(dummyIdLine, dummyDisplayName, dummyTeam, dummyRole);
        dummyPlayer.setId(1L);
        dummyRoom = new Room(0, 0, "LOBBY_STATE", "", 0);
    }

    @Test
    void testEmptyConstructor() {
        Player player = new Player();
        player.setIdLine(dummyIdLine);
        player.setDisplayName(dummyDisplayName);
        player.setTeam(dummyTeam);
        player.setRole(dummyRole);
    }

    @Test
    void testGetId() {
        long expectedOutput = 1L;
        long currentImplOutput = dummyPlayer.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetId() {
        dummyPlayer.setId(2L);
        long expectedOutput = 2L;
        long currentImplOutput = dummyPlayer.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetIdLine() {
        dummyPlayer.setIdLine("dummydummy");
        String expectedOutput = "dummydummy";
        String currentImplOutput = dummyPlayer.getIdLine();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetIdLine() {
        String expectedOutput = "dummy";
        String currentImplOutput = dummyPlayer.getIdLine();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetDisplayName() {
        dummyPlayer.setIdLine("Dummy Dummy");
        String expectedOutput = "Dummy Dummy";
        String currentImplOutput = dummyPlayer.getIdLine();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetDisplayName() {
        String expectedOutput = "Dummy";
        String currentImplOutput = dummyPlayer.getDisplayName();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetTeam() {
        dummyTeam = "Red";
        dummyPlayer.setTeam(dummyTeam);
        String expectedOutput = dummyTeam;
        String currentImplOutput = dummyPlayer.getTeam();

        assertEquals(expectedOutput, currentImplOutput);

        dummyTeam = "Blue";
        dummyPlayer.setTeam(dummyTeam);
        expectedOutput = dummyTeam;
        currentImplOutput = dummyPlayer.getTeam();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetTeam() {
        String expectedOutput = null;
        String currentImplOutput = dummyPlayer.getTeam();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetRole() {
        dummyRole = "Spymaster";
        dummyPlayer.setRole(dummyRole);
        String expectedOutput = dummyRole;
        String currentImplOutput = dummyPlayer.getRole();

        assertEquals(expectedOutput, currentImplOutput);

        dummyRole = "Field  Operator";
        dummyPlayer.setRole(dummyRole);
        expectedOutput = dummyRole;
        currentImplOutput = dummyPlayer.getRole();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetRole() {
        String expectedOutput = null;
        String currentImplOutput = dummyPlayer.getRole();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetRoom() {
        dummyPlayer.setRoom(dummyRoom);
        Room expectedOutput = dummyRoom;
        Room currentImplOutput = dummyPlayer.getRoom();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetRoom() {
        Room expectedOutput = null;
        Room currentImplOutput = dummyPlayer.getRoom();

        assertSame(expectedOutput, currentImplOutput);
    }
}