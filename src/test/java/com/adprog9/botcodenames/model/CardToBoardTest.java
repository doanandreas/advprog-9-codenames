package com.adprog9.botcodenames.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CardToBoardTest {

    private CardToBoard cardToBoard;
    private Card card;
    private Board board;
    private Room room;

    @BeforeEach
    public void setUp() throws Exception {
        this.card = new Card("Handshake");
        this.room = new Room(0, 0, "Start", "nice move", 3);
        this.board = new Board(this.room);
        this.cardToBoard = new CardToBoard(3, "Red Field Operator", this.card, this.board);
    }

    @Test
    void testSetRevealed() {
        cardToBoard.setRevealed(true);
        assertEquals(true, cardToBoard.getRevealed());
    }

    @Test
    void testSetCard() {
        cardToBoard.setCard(this.card);
        assertEquals("Handshake", cardToBoard.getCard().getWord());
    }

    @Test
    void testSetNumberInBoard() {
        cardToBoard.setNumberInBoard(5);
        assertEquals(5, cardToBoard.getNumberInBoard());
    }

    @Test
    void testGetNumberInBoard() {
        assertEquals(3, this.cardToBoard.getNumberInBoard());
    }

    @Test
    void testSetRole() {
        cardToBoard.setRole("Blue Field Operator");
        assertEquals(cardToBoard.getRole(), "Blue Field Operator");
    }

    @Test
    void testGetRole() {
        assertEquals("Red Field Operator", this.cardToBoard.getRole());
    }

    @Test
    void testSetBoard() {
        Board newBoard = new Board(this.room);
        cardToBoard.setBoard(newBoard);
        Board expectedOutput = newBoard;
        Board currentImplOutput = cardToBoard.getBoard();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetBoard() {
        Board expectedOutput = board;
        Board currentImplOutput = cardToBoard.getBoard();

        assertSame(expectedOutput, currentImplOutput);
    }
}