package com.adprog9.botcodenames.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RoomTest {
    int redScore;
    int blueScore;
    int chances;
    String currentState;
    String redFieldOperator;
    String redSpyMaster;
    String blueFieldOperator;
    String blueSpyMaster;
    String currentHint;
    Room dummyRoom;
    String dummyPlayerIdLine;


    @BeforeEach
    void setUp() {
        redScore = 0;
        blueScore = 0;
        chances = 0;
        currentState = "LOBBY_STATE";
        currentHint = "";
        dummyRoom = new Room(redScore, blueScore, currentState, currentHint, chances);
        dummyRoom.setId(1L);
        dummyPlayerIdLine = "dummy";
    }

    @Test
    void testEmptyConstructor() {
        Room room = new Room();
    }

    @Test
    void testGetId() {
        long expectedOutput = 1L;
        long currentImplOutput = dummyRoom.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetId() {
        dummyRoom.setId(2L);
        long expectedOutput = 2L;
        long currentImplOutput = dummyRoom.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }


    @Test
    void testGetPlayers() {
        List<Player> expectedOutput = null;
        List<Player> currentImplOutput = dummyRoom.getPlayers();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetPlayers() {
        List<Player> expectedOutput = new ArrayList<Player>(Arrays.asList(
                new Player(), new Player(), new Player(), new Player()));
        dummyRoom.setPlayers(expectedOutput);
        List<Player> currentImplOutput = dummyRoom.getPlayers();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetRedScore() {
        dummyRoom.setRedScore(1);
        int expectedOutput = 1;
        int currentImplOutput = dummyRoom.getRedScore();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetRedScore() {
        int expectedOutput = 0;
        int currentImplOutput = dummyRoom.getRedScore();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetBlueScore() {
        dummyRoom.setBlueScore(1);
        int expectedOutput = 1;
        int currentImplOutput = dummyRoom.getBlueScore();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetBlueScore() {
        int expectedOutput = 0;
        int currentImplOutput = dummyRoom.getBlueScore();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetCurrentState() {
        dummyRoom.setCurrentState("RED_GUESSING");
        String expectedOutput = "RED_GUESSING";
        String currentImplOutput = dummyRoom.getCurrentState();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetCurrentState() {
        String expectedOutput = "LOBBY_STATE";
        String currentImplOutput = dummyRoom.getCurrentState();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetRedFieldOperator() {
        String expectedOutput = null;
        String currentImplOutput = dummyRoom.getRedFieldOperator();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetRedFieldOperator() {
        String expectedOutput = dummyPlayerIdLine;
        dummyRoom.setRedFieldOperator(dummyPlayerIdLine);
        String currentImplOutput = dummyRoom.getRedFieldOperator();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetRedSpyMaster() {
        String expectedOutput = null;
        String currentImplOutput = dummyRoom.getRedSpyMaster();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetRedSpyMaster() {
        String expectedOutput = dummyPlayerIdLine;
        dummyRoom.setRedSpyMaster(dummyPlayerIdLine);
        String currentImplOutput = dummyRoom.getRedSpyMaster();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetBlueFieldOperator() {
        String expectedOutput = null;
        String currentImplOutput = dummyRoom.getBlueFieldOperator();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetBlueFieldOperator() {
        String expectedOutput = dummyPlayerIdLine;
        dummyRoom.setBlueFieldOperator(dummyPlayerIdLine);
        String currentImplOutput = dummyRoom.getBlueFieldOperator();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetBlueSpyMaster() {
        String expectedOutput = null;
        String currentImplOutput = dummyRoom.getBlueSpyMaster();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetBlueSpyMaster() {
        String expectedOutput = dummyPlayerIdLine;
        dummyRoom.setBlueSpyMaster(dummyPlayerIdLine);
        String currentImplOutput = dummyRoom.getBlueSpyMaster();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetBoard() {
        Board expectedOutput = new Board(dummyRoom);
        dummyRoom.setBoard(expectedOutput);
        Board currentImplOutput = dummyRoom.getBoard();
        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetBoard() {
        Board expectedOutput = null;
        Board currentImplOutput = dummyRoom.getBoard();
        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetCurrentHint() {
        currentHint = "Nairobi";
        dummyRoom.setCurrentHint(currentHint);
        String expectedOutput = currentHint;
        String currentImplOutput = dummyRoom.getCurrentHint();
        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void getCurrentHint() {
        String a = currentHint;
        String b = dummyRoom.getCurrentHint();
        assertEquals(a, b);
    }

    @Test
    void testSetChances() {
        chances = 2;
        dummyRoom.setChances(chances);
        int a = chances;
        int b = dummyRoom.getChances();
        assertEquals(a, b);
    }

    @Test
    void testGetChances() {
        int a = chances;
        int b = dummyRoom.getChances();
        assertEquals(a, b);
    }
}
