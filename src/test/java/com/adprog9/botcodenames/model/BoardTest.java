package com.adprog9.botcodenames.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BoardTest {

    private Board board;
    private Room room;

    @BeforeEach
    public void setUp() {
        this.room = new Room(0, 0, "Start", "nice move", 3);
        this.board = new Board(this.room);
    }

    @Test
    void testGetCardToBoards() {
        int expectedOutput = 0;
        int currentImplOutput = board.getCardToBoards().size();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetCardToBoards() {
        List<CardToBoard> cardToBoard = new ArrayList<>();
        board.setCardToBoards(cardToBoard);
        assertEquals(cardToBoard, this.board.getCardToBoards());
    }

    @Test
    void testGetId() {
        long expectedOutput = 0L;
        long currentImplOutput = board.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetId() {
        board.setId(2L);
        long expectedOutput = 2L;
        long currentImplOutput = board.getId();

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testSetRoom() {
        Room newRoom = new Room();
        board.setRoom(newRoom);
        Room expectedOutput = newRoom;
        Room currentImplOutput = board.getRoom();

        assertSame(expectedOutput, currentImplOutput);
    }

    @Test
    void testGetRoom() {
        Room expectedOutput = room;
        Room currentImplOutput = board.getRoom();

        assertSame(expectedOutput, currentImplOutput);
    }
}