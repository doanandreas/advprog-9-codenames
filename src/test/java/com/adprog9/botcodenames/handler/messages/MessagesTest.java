package com.adprog9.botcodenames.handler.messages;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MessagesTest {

    Messages messages;

    @BeforeEach
    void setUp() {
        messages = new Messages();
    }

    @Test
    void unrecognized() {
        String unrecognizedMessage =
                "Unrecognized Command! Please use /help to see a list of valid commands";
        assertEquals(unrecognizedMessage, messages.UNRECOGNIZED_COMMAND);
    }

    @Test
    void inappropriatePregame() {
        String inappropriatePregameMessage =
                "You can't use that command outside a match!";
        assertEquals(inappropriatePregameMessage, messages.INAPPROPRIATE_COMMAND_FOR_PREGAME);
    }

    @Test
    void inappropriateIngame() {
        String inappropriateIngameMessage =
                "You can't use that command inside a match!";
        assertEquals(inappropriateIngameMessage, messages.INAPPROPRIATE_COMMAND_FOR_INGAME);
    }

    @Test
    void help() {
        String helpMessage = "Here's a list of commands you can use and when you can use them: \n"
                + "CAN BE USED ANYTIME: \n"
                + "/help - Show all valid commands \n"
                + "\n"
                + "PREGAME ONLY: \n"
                + "/createroom - Create a room \n"
                + "/join [roomID] - Join a room with the roomID \n"
                + "/leave - Leave the current room \n"
                + "/tored - Join the red team \n"
                + "/toblue - Join the blue team \n"
                + "/tobench - Leave current team \n"
                + "/bespymaster - Choose role Spymaster for your team \n"
                + "/befieldoperator - Choose role Field Operator for your team \n"
                + "/resetrole - Reset your current role\n"
                + "/status - Check current room status (players and their teams and roles)\n"
                + "/start - Starts the game. Only available when all teams and roles are filled.\n"
                + "\n"
                + "INGAME ONLY: \n"
                + "/peek - Check current board \n"
                + "/score - Check current score \n"
                + "INGAME SPYMASTERS ONLY: \n"
                + "/tell [hint] [howmany] - Create a hint with that many chance to guess \n"
                + "INGAME FIELD OPERATORS ONLY: \n"
                + "/guess [cardnumber] - Guess card with that number \n"
                + "/pass - Pass the turn, if you haven't guessed, automatically lose \n"
                + "/hint - Check the current hint and chances left\n"
                + "/accept - If eligible for One More, accepts it \n"
                + "/decline - If eligible for One More, declines it \n";
        assertEquals(helpMessage, messages.HELP);
    }

}