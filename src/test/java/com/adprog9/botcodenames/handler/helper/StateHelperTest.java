package com.adprog9.botcodenames.handler.helper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import com.adprog9.botcodenames.handler.state.gamestate.*;
import com.adprog9.botcodenames.handler.state.pregamestate.GeneralState;
import com.adprog9.botcodenames.handler.state.pregamestate.LobbyState;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import com.adprog9.botcodenames.repository.PlayerRepository;
import com.adprog9.botcodenames.repository.RoomRepository;
import com.netflix.discovery.converters.Auto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class StateHelperTest {
    String dummyId;
    Player dummyPlayer;
    String roomId;

    @Mock
    GeneralState generalState = new GeneralState();

    @Mock
    LobbyState lobbyState = new LobbyState();

    @Mock
    BlueFieldOperatorState blueFieldOperatorState;

    @Mock
    RedFieldOperatorState redFieldOperatorState;

    @Mock
    BlueSpymasterState blueSpymasterState;

    @Mock
    RedSpymasterState redSpymasterState;

    @Mock
    BlueOneMorePromptState blueOneMorePromptState;

    @Mock
    RedOneMorePromptState redOneMorePromptState;

    @Mock
    BlueOneMoreState blueOneMoreState;

    @Mock
    RedOneMoreState redOneMoreState;

    @Mock
    BlueWinState blueWinState;

    @Mock
    RedWinState redWinState;

    @Mock
    PlayerRepository playerRepository;

    @Mock
    RoomRepository roomRepository;

    @InjectMocks
    StateHelper stateHelper = new StateHelper();

    @BeforeEach
    void setUp() {
        dummyId = "dummy";
        dummyPlayer = new Player(dummyId, "Codenames", null, null);
        roomId = "123";
        when(playerRepository.save(dummyPlayer)).thenReturn(dummyPlayer);
        playerRepository.save(dummyPlayer);
    }

    @Test
    void toGeneralState() {
        assertEquals(null, stateHelper.toState(GeneralState.DB_COL_NAME));
    }

    @Test
    void toLobbyState() {
        assertEquals(lobbyState, stateHelper.toState(LobbyState.DB_COL_NAME));
    }

    @Test
    void toBlueFieldOperatorState() {
        assertEquals(blueFieldOperatorState, stateHelper.toState(blueFieldOperatorState.DB_COL_NAME));
    }

    @Test
    void toRedFieldOperatorState() {
        assertEquals(redFieldOperatorState, stateHelper.toState(redFieldOperatorState.DB_COL_NAME));
    }

    @Test
    void toBlueSpymasterState() {
        assertEquals(blueSpymasterState, stateHelper.toState(blueSpymasterState.DB_COL_NAME));
    }

    @Test
    void toRedSpymasterState() {
        assertEquals(redSpymasterState, stateHelper.toState(redSpymasterState.DB_COL_NAME));
    }

    @Test
    void toBlueOneMorePromptState() {
        assertEquals(blueOneMorePromptState, stateHelper.toState(blueOneMorePromptState.DB_COL_NAME));
    }

    @Test
    void toRedOneMorePromptState() {
        assertEquals(redOneMorePromptState, stateHelper.toState(redOneMorePromptState.DB_COL_NAME));
    }

    @Test
    void toBlueOneMoreState() {
        assertEquals(blueOneMoreState, stateHelper.toState(blueOneMoreState.DB_COL_NAME));
    }

    @Test
    void toRedOneMoreState() {
        assertEquals(redOneMoreState, stateHelper.toState(redOneMoreState.DB_COL_NAME));
    }

    @Test
    void toBlueWinState() {
        assertEquals(blueWinState, stateHelper.toState(blueWinState.DB_COL_NAME));
    }

    @Test
    void toRedWinState() {
        assertEquals(redWinState, stateHelper.toState(redWinState.DB_COL_NAME));
    }

    @Test
    void getUserStateRoomIsNull() {
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        assertEquals(generalState, stateHelper.getUserState(dummyId));
    }

    @Test
    void getUserStateRoomExists() {
        Room dummyRoom = new Room(0, 0, LobbyState.DB_COL_NAME, "", 0);
        roomRepository.save(dummyRoom);
        dummyPlayer.setRoom(dummyRoom);
        playerRepository.save(dummyPlayer);

        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        assertEquals(lobbyState, stateHelper.getUserState(dummyId));
    }
}