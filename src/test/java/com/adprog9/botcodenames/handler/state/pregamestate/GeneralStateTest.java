package com.adprog9.botcodenames.handler.state.pregamestate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.adprog9.botcodenames.generator.RoomGenerator;
import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import com.adprog9.botcodenames.repository.PlayerRepository;
import com.adprog9.botcodenames.repository.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class GeneralStateTest {

    String dummyId;
    Player dummyPlayer;
    String roomId;

    @Mock
    RoomGenerator roomGenerator;

    @Mock
    PlayerRepository playerRepository;

    @Mock
    RoomRepository roomRepository;

    @InjectMocks
    GeneralState state = new GeneralState();

    @BeforeEach
    void setUp() {
        dummyId = "dummy";
        dummyPlayer = new Player(dummyId, "Codenames", null, null);
        roomId = "123";
        when(playerRepository.save(dummyPlayer)).thenReturn(dummyPlayer);
        playerRepository.save(dummyPlayer);
    }

    @Test
    void testHelp() {
        String expectedOutput = Messages.HELP;
        String currentImplOutput = state.help(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testCreateRoom() {
        when(roomGenerator.generateNewRoom()).thenReturn(new Room());
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(new Player());

        String expectedOutput = Messages.SUCCESS_CREATE_ROOM + String.valueOf(0L);
        String currentImplOutput = state.createRoom(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testJoinFull() {
        when(roomRepository.findRoomByIdRoom(123L)).thenReturn(new Room());
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(new Player());
        doReturn(4).when(playerRepository)
                .howManyUsersAreInTheRoom(Mockito.any(Room.class));

        String expectedOutput = Messages.FAIL_JOIN_ROOM_FULL;
        String currentImplOutput = state.join(dummyId, roomId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testJoinNotFull() {
        when(roomRepository.findRoomByIdRoom(123L)).thenReturn(new Room());
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(new Player());
        doReturn(3).when(playerRepository)
                .howManyUsersAreInTheRoom(Mockito.any(Room.class));

        String expectedOutput = Messages.SUCCESS_JOIN_ROOM + String.valueOf(123L);
        String currentImplOutput = state.join(dummyId, roomId);

        assertEquals(expectedOutput, currentImplOutput);
    }


    @Test
    void testLeave() {
        String expectedOutput = Messages.FAIL_LEAVE_ROOM;
        String currentImplOutput = state.leave(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStatus() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.status(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStart() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.start(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRole() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymaster() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperator() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToRed() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.toRed(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBlue() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.toBlue(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBench() {
        String expectedOutput = Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
        String currentImplOutput = state.toBench(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testTell() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.tell(dummyId, "maroco", 1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPeek() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.peek(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testScore() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.score(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuess() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.guess(dummyId, 25);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPass() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.pass(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testHints() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.hints(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testAccept() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.accept(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testDecline() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.decline(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

}
