package com.adprog9.botcodenames.handler.state.gamestate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.adprog9.botcodenames.generator.RoomGenerator;
import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.*;
import com.adprog9.botcodenames.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@ExtendWith(SpringExtension.class)
public class BlueFieldOperatorStateTest {

    String dummyId1;
    Player dummyPlayer1;
    String dummyId2;
    Player dummyPlayer2;
    String dummyId3;
    Player dummyPlayer3;
    String dummyId4;
    Player dummyPlayer4;
    long roomId;

    String dummyHint = "dummy hint";
    int dummyChance = 5;

    @Mock
    RoomRepository roomRepository;

    @Mock
    CardRepository cardRepository;

    @Mock
    CardToBoardRepository cardToBoardRepository;

    @Mock
    BoardRepository boardRepository;

    @Mock
    PlayerRepository playerRepository;

    @InjectMocks
    BlueFieldOperatorState state = new BlueFieldOperatorState();

    @BeforeEach
    void setUp() {
        dummyId1 = "dummy";
        dummyId2 = "dummy2";
        dummyId3 = "dummy3";
        dummyId4 = "dummy4";
        dummyPlayer1 = new Player(dummyId1, "Codenames", null, null);
        dummyPlayer2 = new Player(dummyId2, "Codenames", null, null);
        dummyPlayer3 = new Player(dummyId3, "Codenames", null, null);
        dummyPlayer4 = new Player(dummyId4, "Codenames", null, null);
        roomId = 123;

        when(playerRepository.save(dummyPlayer1)).thenReturn(dummyPlayer1);
        playerRepository.save(dummyPlayer1);

        when(playerRepository.save(dummyPlayer2)).thenReturn(dummyPlayer2);
        playerRepository.save(dummyPlayer2);

        when(playerRepository.save(dummyPlayer3)).thenReturn(dummyPlayer3);
        playerRepository.save(dummyPlayer3);

        when(playerRepository.save(dummyPlayer4)).thenReturn(dummyPlayer4);
        playerRepository.save(dummyPlayer4);
    }

    @Test
    void testHelp() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.help(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testCreateRoom() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.createRoom(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testJoin() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.join(dummyId1, "123");

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testLeave() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.leave(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStatus() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.status(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStart() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.start(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRole() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.resetRole(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymaster() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.beSpymaster(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperator() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.beFieldOperator(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToRed() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.toRed(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBlue() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.toBlue(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBench() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.toBench(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    void prepareCards() {
        Card card1 = new Card("Hollywood");
        Card card2 = new Card("Screen");
        Card card3 = new Card("Play");
        Card card4 = new Card("Marble");
        Card card5 = new Card("Dinosaur");
        Card card6 = new Card("Cat");
        Card card7 = new Card("Pitch");
        Card card8 = new Card("Bond");
        Card card9 = new Card("Greece");
        Card card10 = new Card("Deck");
        Card card11 = new Card("Spike");
        Card card12 = new Card("Center");
        Card card13 = new Card("Vacuum");
        Card card14 = new Card("Unicorn");
        Card card15 = new Card("Undertaker");
        Card card16 = new Card("Sock");
        Card card17 = new Card("Loch Ness");
        Card card18 = new Card("Horse");
        Card card19 = new Card("Berlin");
        Card card20 = new Card("Platypus");
        Card card21 = new Card("Port");
        Card card22 = new Card("Chest");
        Card card23 = new Card("Box");
        Card card24 = new Card("Compound");
        Card card25 = new Card("Ship");

        Card[] cardList = {card1, card2, card3, card4, card5, card6, card7, card8, card9, card10, card11, card12, card13,
                           card14, card15, card16, card17, card18, card19, card20, card21, card22, card23, card24, card25};

        List<Card> cardArrayList = Arrays.asList(cardList);

        for(Card c : cardList) {
            cardRepository.save(c);
        }

        when(cardRepository.findAll()).thenReturn(cardArrayList);
    }

    Room generateRoom() {
        Room room = new Room(0, 0, "LOBBY_STATE", "", 0);
        Board board = new Board(room);
        List<Card> cards = cardRepository.findAll();
//        List<CardToBoard> cardToBoardList = new ArrayList<>();

        for (int cardIndex = 1; cardIndex <= 25; cardIndex++) {
            CardToBoard cardToBoard;

            // Card 1 - 8 tim biru
            if (cardIndex <= 8) {
                cardToBoard = new CardToBoard(cardIndex, "Blue Agent", cards.get(cardIndex-1), board);
                // Card 1 bakal di-reveal dr sananya buat keperluan test
                if(cardIndex == 1) {
                    cardToBoard.setRevealed(true);
                }
            // Card 9 - 17 tim merah
            } else if (cardIndex >= 9 && cardIndex <= 17) {
                cardToBoard = new CardToBoard(cardIndex, "Red Agent", cards.get(cardIndex-1), board);
            // Card 18 assasin
            } else if (cardIndex == 18) {
                cardToBoard = new CardToBoard(cardIndex, "ASSASSIN (!)", cards.get(cardIndex-1), board);
            // Card 19 - 25 innocent
            } else {
                cardToBoard = new CardToBoard(cardIndex, "Innocent", cards.get(cardIndex-1), board);
            }
//            cardToBoardList.add(cardToBoard);
            cardToBoardRepository.save(cardToBoard);
        }

        return room;
    }

    long prepareRoom(String dummyHint, int dummyChance) {
        prepareCards();
        Room room = generateRoom();
        dummyPlayer1.setRoom(room);
        dummyPlayer1.setTeam("Blue");
        dummyPlayer1.setRole("Field Operator");
        playerRepository.save(dummyPlayer1);
        when(playerRepository.findPlayerByUserId(dummyId1)).thenReturn(dummyPlayer1);
        room.setBlueFieldOperator(dummyId1);

        dummyPlayer2.setRoom(room);
        dummyPlayer2.setTeam("Blue");
        dummyPlayer2.setRole("Spymaster");
        playerRepository.save(dummyPlayer2);
        when(playerRepository.findPlayerByUserId(dummyId2)).thenReturn(dummyPlayer2);
        room.setBlueSpyMaster(dummyId2);

        dummyPlayer3.setRoom(room);
        dummyPlayer3.setTeam("Red");
        dummyPlayer3.setRole("Field Operator");
        playerRepository.save(dummyPlayer3);
        when(playerRepository.findPlayerByUserId(dummyId3)).thenReturn(dummyPlayer3);
        room.setRedFieldOperator(dummyId3);

        dummyPlayer4.setRoom(room);
        dummyPlayer4.setTeam("Red");
        dummyPlayer4.setRole("Spymaster");
        playerRepository.save(dummyPlayer4);
        when(playerRepository.findPlayerByUserId(dummyId4)).thenReturn(dummyPlayer4);
        room.setRedSpyMaster(dummyId4);

        room.setCurrentHint(dummyHint);
        room.setChances(dummyChance);
        roomRepository.save(room);
        when(roomRepository.findRoomByIdRoom(room.getId())).thenReturn(room);

        return room.getId();
    }

    @Test
    void testTell() {
        String expectedOutput = Messages.FAIL_USING_SPYMASTER_COMMANDS;
        String currentImplOutput = state.tell(dummyId1, dummyHint, dummyChance);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPeek() {
        long roomId = prepareRoom(dummyHint, dummyChance);

        String expectedOutput = String.format("Current hint: %s - Chances: %s \n",
                dummyHint, dummyChance);

        Player talkingPlayer = playerRepository.findPlayerByUserId(dummyId2);
        Room targetRoom = talkingPlayer.getRoom();
        Board board = targetRoom.getBoard();
        for (CardToBoard ctb : board.getCardToBoards()) {
            String number;
            String word;
            String role = "?";

            number = String.valueOf(ctb.getNumberInBoard());
            word = ctb.getCard().getWord();

            if (ctb.getRevealed()) {
                role = ctb.getRole();
            }
            expectedOutput = expectedOutput + String.format("%s) %s - %s \n",
                    number, word, role);
        }

        String currentImplOutput = state.peek(dummyId2);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testScore() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.score(dummyId1);

        //assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessAlreadyRevealed() {
        prepareRoom(dummyHint, dummyChance);

        String expectedOutput = Messages.CARD_ALREADY_GUESSED;
        String currentImplOutput = state.guess(dummyId1, 1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessBlueAgent() {
        prepareRoom(dummyHint, dummyChance);

        Room targetRoom = dummyPlayer1.getRoom();
        int roomChances = targetRoom.getChances();
        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();

        String responses = String.format("%s %s \n"
                        + "%s \n"
                        + "Blue Score: %s \n"
                        + "Red Score: %s",
                Messages.BLUE_GUESS, 2,
                Messages.ITS_BLUE, blueScore + 1, redScore);
        String expectedOutput = String.format("push;%s;%s %s %s %s",
                responses, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.guess(dummyId1, 2);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessBlueAgentAndWins() {
        prepareRoom(dummyHint, dummyChance);

        Room targetRoom = dummyPlayer1.getRoom();
        targetRoom.setBlueScore(8);
        int roomChances = targetRoom.getChances();
        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();

        String responses = String.format("%s %s \n"
                        + "%s \n"
                        + "Blue Score: %s \n"
                        + "Red Score: %s",
                Messages.BLUE_GUESS, 2,
                Messages.ITS_BLUE, blueScore + 1, redScore) + String.format("\n%s", Messages.BLUE_WINS_NORMAL);;
        String expectedOutput = String.format("push;%s;%s %s %s %s",
                responses, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.guess(dummyId1, 2);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessBlueAgentAndOneMoreEligible() {
        prepareRoom(dummyHint, dummyChance);

        Room targetRoom = dummyPlayer1.getRoom();
        targetRoom.setChances(1);
        int roomChances = targetRoom.getChances();
        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();

        String responses = String.format("%s %s \n"
                        + "%s \n"
                        + "Blue Score: %s \n"
                        + "Red Score: %s",
                Messages.BLUE_GUESS, 2,
                Messages.ITS_BLUE, blueScore + 1, redScore) + String.format("\n%s", Messages.BLUE_CAN_ONE_MORE);;
        String expectedOutput = String.format("push;%s;%s %s %s %s",
                responses, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.guess(dummyId1, 2);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessRedAgent() {
        prepareRoom(dummyHint, dummyChance);

        Room targetRoom = dummyPlayer1.getRoom();
        int roomChances = targetRoom.getChances();
        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();

        String responses = String.format("%s %s \n"
                        + "%s \n"
                        + "Blue Score: %s \n"
                        + "Red Score: %s",
                Messages.BLUE_GUESS, 9, Messages.ITS_RED,
                blueScore, redScore + 1) + String.format("\n%s", Messages.RED_SPYMASTER_START);
        String expectedOutput = String.format("push;%s;%s %s %s %s",
                responses, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.guess(dummyId1, 9);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessRedAgentAndRedWins() {
        prepareRoom(dummyHint, dummyChance);

        Room targetRoom = dummyPlayer1.getRoom();
        targetRoom.setRedScore(7);
        int roomChances = targetRoom.getChances();
        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();

        String responses = String.format("%s %s \n"
                        + "%s \n"
                        + "Blue Score: %s \n"
                        + "Red Score: %s",
                Messages.BLUE_GUESS, 9, Messages.ITS_RED,
                blueScore, redScore + 1) + String.format("\n%s", Messages.RED_WINS_NORMAL);;
        String expectedOutput = String.format("push;%s;%s %s %s %s",
                responses, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.guess(dummyId1, 9);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessAssasin() {
        prepareRoom(dummyHint, dummyChance);

        Room targetRoom = dummyPlayer1.getRoom();
        int roomChances = targetRoom.getChances();
        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();

        String responses = String.format("%s %s \n"
                + "%s", Messages.BLUE_GUESS, 18, Messages.RED_WINS_ASSASSIN);
        String expectedOutput = String.format("push;%s;%s %s %s %s",
                responses, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.guess(dummyId1, 18);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessCivilian() {
        prepareRoom(dummyHint, dummyChance);

        Room targetRoom = dummyPlayer1.getRoom();
        int roomChances = targetRoom.getChances();
        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();

        String responses = String.format("%s %s \n"
                        + "%s \n"
                        + "Blue Score: %s \n"
                        + "Red Score: %s",
                Messages.BLUE_GUESS, 19,
                Messages.ITS_CIVILIAN, blueScore, redScore) + String.format("\n%s", Messages.RED_SPYMASTER_START);;
        String expectedOutput = String.format("push;%s;%s %s %s %s",
                responses, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.guess(dummyId1, 19);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuessBukanBlueFO() {
        prepareRoom(dummyHint, dummyChance);

        String expectedOutput = Messages.FAIL_UNAUTHORIZED + "Blue Field Operator";
        String currentImplOutput = state.guess(dummyId2, 1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPassBlueFO() {
        prepareRoom(dummyHint, dummyChance);

        String expectedOutput = Messages.BLUE_PASS + "\n" + Messages.RED_SPYMASTER_START;
        String expectedOutputWithPlayerId = String.format("push;%s;%s %s %s %s",
                expectedOutput, dummyId3, dummyId4, dummyId1, dummyId2);
        String currentImplOutput = state.pass(dummyId1);

        assertEquals(expectedOutputWithPlayerId, currentImplOutput);
    }

    @Test
    void testPassBukanBlueFO() {
        prepareRoom(dummyHint, dummyChance);

        String expectedOutput = Messages.FAIL_UNAUTHORIZED + "Blue Field Operator";
        String currentImplOutput = state.pass(dummyId4);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testHints() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.hints(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testAccept() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.accept(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testDecline() {
        String expectedOutput = Messages.FAIL_USING_NONGAME_COMMANDS;
        String currentImplOutput = state.decline(dummyId1);

        assertEquals(expectedOutput, currentImplOutput);
    }
}
