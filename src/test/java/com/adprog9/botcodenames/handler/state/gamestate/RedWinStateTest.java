package com.adprog9.botcodenames.handler.state.gamestate;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RedWinStateTest {

    String dummyId;
    Player dummyPlayer;
    String roomId;

    RedWinState state = new RedWinState();

    @BeforeEach
    void setUp() {
        dummyId = "dummy";
        dummyPlayer = new Player(dummyId, "Codenames", null, null);
        roomId = "123";
    }

    @Test
    void testHelp() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.help(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testCreateRoom() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.createRoom(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testJoin() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.join(dummyId, roomId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    // @Test
    // void testLeave() {
    //     String expectedOutput = Messages.RED_HAS_WON;
    //     String currentImplOutput = state.leave(dummyId);

    //     assertEquals(expectedOutput, currentImplOutput);
    // }

    @Test
    void testStatus() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.status(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStart() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.start(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRole() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymaster() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperator() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToRed() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.toRed(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBlue() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.toBlue(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBench() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.toBench(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testTell() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.tell(dummyId, "maroco", 1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPeek() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.peek(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testScore() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.score(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuess() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.guess(dummyId, 25);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPass() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.pass(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testHints() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.hints(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testAccept() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.accept(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testDecline() {
        String expectedOutput = Messages.RED_HAS_WON;
        String currentImplOutput = state.decline(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }
}
