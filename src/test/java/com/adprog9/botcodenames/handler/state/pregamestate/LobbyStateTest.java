package com.adprog9.botcodenames.handler.state.pregamestate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import com.adprog9.botcodenames.generator.RoomGenerator;
import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Board;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import com.adprog9.botcodenames.repository.PlayerRepository;
import com.adprog9.botcodenames.repository.RoomRepository;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
public class LobbyStateTest {
    String dummyId;
    Player dummyPlayer;
    String roomId;

    @Mock
    RoomGenerator roomGenerator;

    @Mock
    PlayerRepository playerRepository;

    @Mock
    RoomRepository roomRepository;

    @InjectMocks
    LobbyState state = new LobbyState();

    @BeforeEach
    void setUp() {
        dummyId = "dummy";
        dummyPlayer = new Player(dummyId, "Codenames", null, null);
        roomId = "123";
        when(playerRepository.save(dummyPlayer)).thenReturn(dummyPlayer);
        playerRepository.save(dummyPlayer);
    }

    @Test
    void testHelp() {
        String expectedOutput = Messages.HELP;
        String currentImplOutput = state.help(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testCreateRoom() {
        String expectedOutput = Messages.FAIL_CREATE_ROOM;
        String currentImplOutput = state.createRoom(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testJoin() {
        String expectedOutput = Messages.FAIL_JOIN_ROOM;
        String currentImplOutput = state.join(dummyId, roomId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testLeaveWithoutRole() {
        Room room = new Room();
        Board board = new Board(room);
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        doReturn(0).when(playerRepository)
                .howManyUsersAreInTheRoom(Mockito.any(Room.class));

        String expectedOutput = Messages.SUCCESS_LEAVE_ROOM;
        //String currentImplOutput = state.leave(dummyId);

        //assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testLeaveRedFieldOperator() {
        Room room = new Room();
        Board board = new Board(room);
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Field Operator");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        doReturn(1).when(playerRepository).howManyUsersAreInTheRoom(Mockito.any(Room.class));

        String expectedOutput = Messages.SUCCESS_LEAVE_ROOM;
        String currentImplOutput = state.leave(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testLeaveRedSpymaster() {
        Room room = new Room();
        Board board = new Board(room);
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        doReturn(1).when(playerRepository).howManyUsersAreInTheRoom(Mockito.any(Room.class));

        String expectedOutput = Messages.SUCCESS_LEAVE_ROOM;
        String currentImplOutput = state.leave(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testLeaveBlueFieldOperator() {
        Room room = new Room();
        Board board = new Board(room);
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Field Operator");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        doReturn(3).when(playerRepository).howManyUsersAreInTheRoom(Mockito.any(Room.class));

        String expectedOutput = Messages.SUCCESS_LEAVE_ROOM;
        String currentImplOutput = state.leave(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testLeaveBlueSpymaster() {
        Room room = new Room();
        Board board = new Board(room);
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        doReturn(2).when(playerRepository).howManyUsersAreInTheRoom(Mockito.any(Room.class));

        String expectedOutput = Messages.SUCCESS_LEAVE_ROOM;
        String currentImplOutput = state.leave(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRoleNoRole() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.SUCCESS_RESET;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRoleNoTeam() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.SUCCESS_RESET;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRoleRedFieldOperator() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Field Operator");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.SUCCESS_RESET;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRoleRedSpyMaster() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.SUCCESS_RESET;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRoleBlueFieldOperator() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Field Operator");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.SUCCESS_RESET;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testResetRoleBlueSpyMaster() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.SUCCESS_RESET;
        String currentImplOutput = state.resetRole(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterNoTeamFail() {
        Room room = new Room();
        room.setRedSpyMaster("abc");
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterRedFail() {
        Room room = new Room();
        room.setRedSpyMaster("abc");
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterBlueFail() {
        Room room = new Room();
        room.setBlueSpyMaster("abc");
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterRedNoRole() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterRedFromFieldOperator() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Field Operator");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterBlueNoRole() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterBlueFromFieldOperator() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Field Operator");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeSpymasterPlayerRoomNull() {
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_SPYMASTER;
        String currentImplOutput = state.beSpymaster(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }


    @Test
    void testBeFieldOperatorNoTeamFail() {
        Room room = new Room();
        room.setRedFieldOperator("abc");
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperatorRedFail() {
        Room room = new Room();
        room.setRedFieldOperator("abc");
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperatorBlueFail() {
        Room room = new Room();
        room.setBlueFieldOperator("abc");
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperatorRedNoRole() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperatorRedFromSpymaster() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperatorBlueNoRole() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperatorBlueFromSpymaster() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.SUCCESS_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testBeFieldOperatorPlayerRoomNull() {
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.FAIL_BE_FIELD_OPERATOR;
        String currentImplOutput = state.beFieldOperator(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToRedSuccess() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(playerRepository.howManyUsersJoinedThisTeamInThisRoom(room, "Red")).thenReturn(1);

        String expectedOutput = Messages.SUCCESS_JOIN_RED;
        String currentImplOutput = state.toRed(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToRedFailFull() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(playerRepository.howManyUsersJoinedThisTeamInThisRoom(room, "Red")).thenReturn(2);

        String expectedOutput = Messages.FAIL_JOIN_RED;
        String currentImplOutput = state.toRed(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToRedFailHasRole() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(playerRepository.howManyUsersJoinedThisTeamInThisRoom(room, "Red")).thenReturn(1);

        String expectedOutput = Messages.CANT_CHANGE_TEAM_HAS_ROLE + dummyPlayer.getRole();
        String currentImplOutput = state.toRed(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBlueSuccess() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(playerRepository.howManyUsersJoinedThisTeamInThisRoom(room, "Blue")).thenReturn(1);

        String expectedOutput = Messages.SUCCESS_JOIN_BLUE;
        String currentImplOutput = state.toBlue(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBlueFailFull() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(playerRepository.howManyUsersJoinedThisTeamInThisRoom(room, "Blue")).thenReturn(2);

        String expectedOutput = Messages.FAIL_JOIN_BLUE;
        String currentImplOutput = state.toBlue(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBlueFailHasRole() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(playerRepository.howManyUsersJoinedThisTeamInThisRoom(room, "Blue")).thenReturn(1);

        String expectedOutput = Messages.CANT_CHANGE_TEAM_HAS_ROLE + dummyPlayer.getRole();
        String currentImplOutput = state.toBlue(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }


    @Test
    void testToBenchSuccess() {
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole(null);
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.SUCCESS_MOVE_TO_BENCH;
        String currentImplOutput = state.toBench(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testToBenchFail() {
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("SpyMaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        String expectedOutput = Messages.CANT_CHANGE_TEAM_HAS_ROLE + dummyPlayer.getRole();
        String currentImplOutput = state.toBench(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStartSuccess() {
        Room room = new Room(0, 0, "LOBBY_STATE", "", 0);
        String redFO = "RedFO";
        String redSM = "RedSM";
        String blueFO = "BlueFO";
        String blueSM = "BlueSM";
        room.setRedFieldOperator(redFO);
        room.setRedSpyMaster(redSM);
        room.setBlueFieldOperator(blueFO);
        room.setBlueSpyMaster(blueSM);

        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = String.format("push;%s;%s %s %s %s",
                Messages.SUCCESS_START, blueFO, blueSM, redFO, redSM);
        String currentImplOutput = state.start(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStartFail() {
        Room room = new Room(0, 0, "LOBBY_STATE", "", 0);
        room.setRedFieldOperator("RedFO");
        room.setRedSpyMaster("RedSM");
        room.setBlueFieldOperator("BlueFO");
        room.setBlueSpyMaster(null);

        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Red");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);
        when(roomRepository.save(room)).thenReturn(room);

        String expectedOutput = Messages.FAIL_START;
        String currentImplOutput = state.start(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testStatus() {
        Room room = new Room();
        dummyPlayer.setRoom(room);
        dummyPlayer.setTeam("Blue");
        dummyPlayer.setRole("Spymaster");
        playerRepository.save(dummyPlayer);
        when(playerRepository.findPlayerByUserId(dummyId)).thenReturn(dummyPlayer);

        Player dummyPlayer2 = new Player("dummy2", "Number 2", null, null);
        dummyPlayer2.setRoom(room);
        room.setPlayers(new ArrayList<Player>(Arrays.asList(dummyPlayer, dummyPlayer2)));


        String expectedOutput = "Here's who's inside and what role they play: \n"
            + "Room ID: 0 \n"
            + "Codenames - Blue - Spymaster\n"
            + "Number 2 - Bench - Unassigned Role\n";
        String currentImplOutput = state.status(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }


    @Test
    void testTell() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.tell(dummyId, "maroco", 1);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPeek() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.peek(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testScore() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.score(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testGuess() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.guess(dummyId, 25);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testPass() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.pass(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testHints() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.hints(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testAccept() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.accept(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }

    @Test
    void testDecline() {
        String expectedOutput = Messages.FAIL_USING_GAME_COMMANDS;
        String currentImplOutput = state.decline(dummyId);

        assertEquals(expectedOutput, currentImplOutput);
    }
}
