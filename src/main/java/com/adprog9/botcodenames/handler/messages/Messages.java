package com.adprog9.botcodenames.handler.messages;

public class Messages {
    public static final String UNRECOGNIZED_COMMAND =
            "Unrecognized Command! Please use /help to see a list of valid commands";

    public static final String INAPPROPRIATE_COMMAND_FOR_PREGAME =
            "You can't use that command outside a match!";

    public static final String INAPPROPRIATE_COMMAND_FOR_INGAME =
            "You can't use that command inside a match!";

    public static final String HELP =
            "Here's a list of commands you can use and when you can use them: \n"
                    + "CAN BE USED ANYTIME: \n"
                    + "/help - Show all valid commands \n"
                    + "\n"
                    + "PREGAME ONLY: \n"
                    + "/createroom - Create a room \n"
                    + "/join [roomID] - Join a room with the roomID \n"
                    + "/leave - Leave the current room \n"
                    + "/tored - Join the red team \n"
                    + "/toblue - Join the blue team \n"
                    + "/tobench - Leave current team \n"
                    + "/bespymaster - Choose role Spymaster for your team \n"
                    + "/befieldoperator - Choose role Field Operator for your team \n"
                    + "/resetrole - Reset your current role\n"
                    + "/status - Check current room status (players and their teams and roles)\n"
                    + "/start - Starts the game. Only available when all teams and roles are "
                    + "filled.\n"
                    + "\n"
                    + "INGAME ONLY: \n"
                    + "/peek - Check current board \n"
                    + "/score - Check current score \n"
                    + "INGAME SPYMASTERS ONLY: \n"
                    + "/tell [hint] [howmany] - Create a hint with that many chance to guess \n"
                    + "INGAME FIELD OPERATORS ONLY: \n"
                    + "/guess [cardnumber] - Guess card with that number \n"
                    + "/pass - Pass the turn, if you haven't guessed, automatically lose \n"
                    + "/hint - Check the current hint and chances left\n"
                    + "/accept - If eligible for One More, accepts it \n"
                    + "/decline - If eligible for One More, declines it \n";
    public static final String FAIL_CREATE_ROOM = "You are already inside a room. "
            + "Please leave this room first if you want to create a new room.";

    public static final String FAIL_JOIN_ROOM = "You are already inside a room. "
            + "Please leave this room first if you want to join a new room.";

    public static final String FAIL_JOIN_ROOM_FULL = "The room is full!";

    public static final String FAIL_LEAVE_ROOM = "You are not inside a game room right now, "
            + "please join or create a room first.";

    public static final String FAIL_USING_INSIDE_ROOM_COMMANDS =
            "You are not inside a room right now, "
            + "please join or create a room first.";

    public static final String FAIL_USING_OUTSIDE_ROOM_COMMANDS =
            "You are inside a room right now, "
            + "you can't use that command.";

    public static final String FAIL_USING_GAME_COMMANDS = "You are not playing a game right now, "
            + "you can't use that command.";

    public static final String FAIL_USING_NONGAME_COMMANDS = "You are playing a game right now, "
            + "you can't use that command.";

    public static final String FAIL_USING_SPYMASTER_COMMANDS = "You are a Field Operator, "
            + "you can't use that command.";

    public static final String FAIL_USING_FIELDOPERATOR_COMMANDS = "You are a Spymaster, "
            + "you can't use that command.";

    public static final String SUCCESS_CREATE_ROOM = "Room successfully created!\n"
            + "Your Room ID:";

    public static final String SUCCESS_JOIN_ROOM = "Successfully joined to Room with ID: ";

    public static final String SUCCESS_LEAVE_ROOM = "You left a Room with ID: ";

    public static final String COMING_SOON =
            "Sorry, this feature is still under development! Please come back later :)";

    public static final String SUCCESS_BE_SPYMASTER = "You're now a Spymaster for your team!";

    public static final String FAIL_BE_SPYMASTER = "You can't be a Spymaster now...";

    public static final String SUCCESS_BE_FIELD_OPERATOR =
            "You're now a Field Operator for your team!";

    public static final String FAIL_BE_FIELD_OPERATOR = "You can't be a Field Operator now...";

    public static final String SUCCESS_JOIN_RED = "You're a member of The Red Team now!";

    public static final String SUCCESS_JOIN_BLUE = "You're a member of The Blue Team now!";

    public static final String FAIL_JOIN_RED = "You can't join The Red Team...";

    public static final String FAIL_JOIN_BLUE = "You can't join The Blue Team...";

    public static final String SUCCESS_MOVE_TO_BENCH = "You moved to The Bench";

    public static final String FAIL_START = "You can't start the game yet.";

    public static final String CANT_CHANGE_TEAM_HAS_ROLE =
            "You can't change/leave your team, you have been assigned a role as a ";

    public static final String ROOM_SCORE = "Score for this room:";

    public static final String FAIL_UNAUTHORIZED =
            "You're not allowed to use this command because you're not ";

    public static final String THIS_IS_RED_PROMPT =
            "You're not allowed to use this command, only Red Field Operator can act here!\n"
            + "And it is limited to /accept and /decline!";

    public static final String THIS_IS_BLUE_PROMPT =
            "You're not allowed to use this command, only Blue Field Operator can act here!\n"
            + "And it is limited to /accept and /decline!";

    public static final String THIS_IS_RED_OM =
            "You're not allowed to use this command, only Red Field Operator can act here!\n"
            + "And it is limited to guessing!";

    public static final String THIS_IS_BLUE_OM =
            "You're not allowed to use this command, only Blue Field Operator can act here!\n"
            + "And it is limited to guessing!";

    public static final String NO_BACKING =
            "You've opted to use your One More chance, you can't back down now.";

    public static final String RED_ACCEPTS =
            "The Red Field Operator is eager for One More!";

    public static final String RED_DECLINES =
            "The Red Field Operator declines the offer for One More.";

    public static final String BLUE_ACCEPTS =
            "The Blue Field Operator is eager for One More!";

    public static final String BLUE_DECLINES =
            "The Blue Field Operator declines the offer for One More.";

    //Also for Push Messages
    public static final String SUCCESS_START = "The game begins!";

    public static final String STATUS = "Here's who's inside and what role they play: ";

    public static final String SUCCESS_RESET = "Your role has been reset.";

    public static final String TELL_RED = "Red Spymaster is telling: ";

    public static final String TELL_BLUE = "Blue Spymaster is telling: ";

    public static final String RED_SPYMASTER_START =
            "Now it's time for Red Spymaster to brew up a hint!";

    public static final String BLUE_SPYMASTER_START =
            "Now it's time for Blue Spymaster to brew up a hint!";

    public static final String BLUE_FIELD_OPERATOR_START =
            "Now it's time for Blue Field Operator to guess!";

    public static final String RED_FIELD_OPERATOR_START =
            "Now it's time for Red Field Operator to guess!";

    public static final String RED_CAN_ONE_MORE =
            "Red Field Operator didn't waste any chances! They can opt for one more!";

    public static final String BLUE_CAN_ONE_MORE =
            "Blue Field Operator didn't waste any chances! They can opt for one more!";

    public static final String CARD_ALREADY_GUESSED =
            "You can't pick that card! It's already guessed!";

    public static final String RED_GUESS = "Red Field Operator guessed card #";

    public static final String BLUE_GUESS = "Blue Field Operator guessed card #";

    public static final String RED_PASS = "Red Field Operator decides to pass.";

    public static final String BLUE_PASS = "Blue Field Operator decides to pass.";

    public static final String ITS_RED = "It's a Red Agent!";

    public static final String ITS_BLUE = "It's a Blue Agent!";

    public static final String ITS_CIVILIAN = "It's a Civilian!";

    public static final String DEBUG_FO = "Let's begin the test, for real";

    public static final String RED_WINS_NORMAL =
            "The guess seals it! The Red Team wins!!!";

    public static final String BLUE_WINS_NORMAL =
            "The guess seals it! The Blue Team wins!!!";

    public static final String RED_WINS_ASSASSIN =
            "Poor Blue Field Operator, you have picked the Assassin! Causing The Red Team to Win!";

    public static final String BLUE_WINS_ASSASSIN =
            "Poor Red Field Operator, you have picked the Assassin! Causing The Blue Team to Win!";

    public static final String RED_HAS_WON =
            "Nothing left to do, for The Red Team has won! Please use /leave to leave the room.";

    public static final String BLUE_HAS_WON =
            "Nothing left to do, for The Blue Team has won! Please use /leave to leave the room.";
}
