package com.adprog9.botcodenames.handler.state.gamestate;


import com.adprog9.botcodenames.generator.RoomGenerator;
import com.adprog9.botcodenames.repository.CardRepository;
import com.adprog9.botcodenames.repository.PlayerRepository;
import com.adprog9.botcodenames.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class GameState {
    @Autowired
    protected PlayerRepository playerRepository;

    @Autowired
    protected CardRepository cardRepository;

    @Autowired
    protected RoomRepository roomRepository;

    protected RoomGenerator roomGenerator = new RoomGenerator();


    // Pre-game commands
    public abstract String help(String userId);

    public abstract String createRoom(String userId);

    public abstract String join(String userId, String roomId);

    public abstract String leave(String userId);

    public abstract String resetRole(String userId);

    public abstract String beSpymaster(String userId);

    public abstract String beFieldOperator(String userId);

    public abstract String toRed(String userId);

    public abstract String toBlue(String userId);

    public abstract String toBench(String userId);

    public abstract String start(String userId);

    public abstract String status(String userId);


    // In-game commands
    protected String responses;

    public abstract String tell(String userId, String hint, int chances);

    public abstract String peek(String userId);

    public abstract String score(String userId);

    public abstract String guess(String userId, int number);

    public abstract String pass(String userId);

    public abstract String hints(String userId);

    public abstract String accept(String userId);

    public abstract String decline(String userId);

}
