package com.adprog9.botcodenames.handler.state.gamestate;


import com.adprog9.botcodenames.handler.messages.Messages;
import org.springframework.stereotype.Component;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import com.adprog9.botcodenames.repository.PlayerRepository;
import com.adprog9.botcodenames.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;


@Component
public class BlueWinState extends GameState {
    public static final String DB_COL_NAME = "BLUE_ASSASSINATED";

    public BlueWinState() {
        this.responses = "";
    }

    //Pre-game Commands
    @Override
    public String help(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String createRoom(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String join(String userId, String roomId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String leave(String userId) {
        //TODO
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        if (talkingPlayer.getRole() != null) {
            if (talkingPlayer.getRole().equals("Spymaster")) {
                if (talkingPlayer.getTeam().equals("Red")) {
                    talkingPlayer.getRoom().setRedSpyMaster(null);
                } else {
                    talkingPlayer.getRoom().setBlueSpyMaster(null);
                }
            } else {
                if (talkingPlayer.getTeam().equals("Red")) {
                    talkingPlayer.getRoom().setRedFieldOperator(null);
                } else {
                    talkingPlayer.getRoom().setBlueFieldOperator(null);
                }
            }
        }

        talkingPlayer.setRole(null);
        talkingPlayer.setTeam(null);

        playerRepository.save(talkingPlayer);

        Room targetRoom = talkingPlayer.getRoom();
        talkingPlayer.setRoom(null);
        if (playerRepository.howManyUsersAreInTheRoom(targetRoom) == 0) {
            Long roomId = targetRoom.getId();
            targetRoom.getBoard().setRoom(null);
            targetRoom.setBoard(null);
            roomRepository.deleteRoom(roomId);
        }

        return Messages.SUCCESS_LEAVE_ROOM;
    }

    @Override
    public String resetRole(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String beSpymaster(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String beFieldOperator(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String toRed(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String toBlue(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String toBench(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String start(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String status(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    public void emptyResponseList() {
        responses = "";
    }


    //In-game Commands
    @Override
    public String tell(String userId, String hint, int chances) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String peek(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String score(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String guess(String userId, int number) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String pass(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String hints(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String accept(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }

    @Override
    public String decline(String userId) {
        emptyResponseList();
        responses = Messages.BLUE_HAS_WON;
        return this.responses;
    }
}
