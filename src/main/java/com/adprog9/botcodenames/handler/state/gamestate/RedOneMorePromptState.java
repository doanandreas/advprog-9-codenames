package com.adprog9.botcodenames.handler.state.gamestate;


import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import org.springframework.stereotype.Component;

@Component
public class RedOneMorePromptState extends GameState {
    public static final String DB_COL_NAME = "RED_ONE_MORE_PROMPT";

    public RedOneMorePromptState() {
        this.responses = "";
    }

    //Pre-game Commands
    @Override
    public String help(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String createRoom(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String join(String userId, String roomId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String leave(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String resetRole(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String beSpymaster(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String beFieldOperator(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String toRed(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String toBlue(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String toBench(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String start(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String status(String userId) {
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    public void emptyResponseList() {
        responses = "";
    }


    //In-game Commands
    @Override
    public String tell(String userId, String hint, int chances) {
        // TO-DO
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String peek(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String score(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String guess(String userId, int number) {
        // TO-DO
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String pass(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String hints(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.THIS_IS_RED_PROMPT;
        return this.responses;
    }

    @Override
    public String accept(String userId) {
        // TO-DO
        emptyResponseList();

        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();
        String blueFOPlayer = targetRoom.getBlueFieldOperator();
        String blueSMPlayer = targetRoom.getBlueSpyMaster();
        String redFOPlayer = targetRoom.getRedFieldOperator();
        String redSMPlayer = targetRoom.getRedSpyMaster();

        if (talkingPlayer.getRole().equals("Field Operator")
                && talkingPlayer.getTeam().equals("Red")) {
            targetRoom.setCurrentState(RedOneMoreState.DB_COL_NAME);
            roomRepository.save(targetRoom);
            responses = Messages.RED_ACCEPTS;
            String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                    responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
            return messageWithPlayerID;
        } else {
            responses = Messages.THIS_IS_RED_PROMPT;
            return this.responses;
        }


    }

    @Override
    public String decline(String userId) {
        emptyResponseList();

        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();
        String blueFOPlayer = targetRoom.getBlueFieldOperator();
        String blueSMPlayer = targetRoom.getBlueSpyMaster();
        String redFOPlayer = targetRoom.getRedFieldOperator();
        String redSMPlayer = targetRoom.getRedSpyMaster();

        if (talkingPlayer.getRole().equals("Field Operator")
                && talkingPlayer.getTeam().equals("Red")) {
            targetRoom.setCurrentState(BlueSpymasterState.DB_COL_NAME);
            roomRepository.save(targetRoom);
            responses = Messages.RED_DECLINES + "\n" + Messages.BLUE_SPYMASTER_START;
            String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                    responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
            return messageWithPlayerID;
        } else {
            responses = Messages.THIS_IS_RED_PROMPT;
            return this.responses;
        }
    }
}
