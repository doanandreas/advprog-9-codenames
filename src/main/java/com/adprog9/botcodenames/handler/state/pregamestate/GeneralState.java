package com.adprog9.botcodenames.handler.state.pregamestate;


import com.adprog9.botcodenames.generator.RoomGenerator;
import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.handler.state.gamestate.GameState;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import com.adprog9.botcodenames.repository.PlayerRepository;
import com.adprog9.botcodenames.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeneralState extends GameState {
    public static final String DB_COL_NAME = "OUTSIDE_ROOM";

    @Autowired
    RoomGenerator roomGenerator;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    RoomRepository roomRepository;

    public GeneralState() {
        this.responses = "";
    }

    // Pregame Commands
    @Override
    public String help(String userId) {
        return Messages.HELP;
    }


    @Override
    public String createRoom(String userId) {
        // TO-DO
        emptyResponseList();
        Room newRoom = roomGenerator.generateNewRoom();
        System.out.println(newRoom == null);
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        talkingPlayer.setRoom(newRoom);
        String roomIdInfo = String.valueOf(newRoom.getId());
        responses = Messages.SUCCESS_CREATE_ROOM + roomIdInfo;
        playerRepository.save(talkingPlayer);
        return this.responses;
    }

    @Override
    public String join(String userId, String roomId) {
        Long roomIdL = Long.parseLong(roomId);
        emptyResponseList();
        Room targetRoom = roomRepository.findRoomByIdRoom(roomIdL);
        if (playerRepository.howManyUsersAreInTheRoom(targetRoom) == 4) {
            return Messages.FAIL_JOIN_ROOM_FULL;
        } else {
            Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
            talkingPlayer.setRoom(targetRoom);
            playerRepository.save(talkingPlayer);
            return Messages.SUCCESS_JOIN_ROOM + roomId;
        }
    }

    @Override
    public String leave(String userId) {
        return Messages.FAIL_LEAVE_ROOM;
    }

    @Override
    public String status(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    @Override
    public String start(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    @Override
    public String resetRole(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    @Override
    public String beSpymaster(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    @Override
    public String beFieldOperator(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    @Override
    public String toRed(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    @Override
    public String toBlue(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    @Override
    public String toBench(String userId) {
        return Messages.FAIL_USING_INSIDE_ROOM_COMMANDS;
    }

    public void emptyResponseList() {
        responses = "";
    }

    // In-game Commands
    @Override
    public String tell(String userId, String hint, int chances) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String peek(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String score(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String guess(String userId, int number) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String pass(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String hints(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String accept(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String decline(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }
}
