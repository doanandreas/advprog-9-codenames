package com.adprog9.botcodenames.handler.state.pregamestate;


import com.adprog9.botcodenames.generator.RoomGenerator;
import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.handler.state.gamestate.GameState;
import com.adprog9.botcodenames.handler.state.gamestate.RedSpymasterState;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import com.adprog9.botcodenames.repository.PlayerRepository;
import com.adprog9.botcodenames.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LobbyState extends GameState {
    public static final String DB_COL_NAME = "LOBBY_STATE";

    @Autowired
    RoomGenerator roomGenerator;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    RoomRepository roomRepository;

    public LobbyState() {
        this.responses = "";
    }

    // Pregame Commands
    @Override
    public String help(String userId) {
        return Messages.HELP;
    }

    @Override
    public String createRoom(String userId) {
        return Messages.FAIL_CREATE_ROOM;
    }

    @Override
    public String join(String userId, String roomId) {
        return Messages.FAIL_JOIN_ROOM;
    }

    @Override
    public String leave(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        if (talkingPlayer.getRole() != null) {
            if (talkingPlayer.getRole().equals("Spymaster")) {
                if (talkingPlayer.getTeam().equals("Red")) {
                    talkingPlayer.getRoom().setRedSpyMaster(null);
                } else {
                    talkingPlayer.getRoom().setBlueSpyMaster(null);
                }
            } else {
                if (talkingPlayer.getTeam().equals("Red")) {
                    talkingPlayer.getRoom().setRedFieldOperator(null);
                } else {
                    talkingPlayer.getRoom().setBlueFieldOperator(null);
                }
            }
        }

        talkingPlayer.setRole(null);
        talkingPlayer.setTeam(null);

        playerRepository.save(talkingPlayer);

        Room targetRoom = talkingPlayer.getRoom();
        talkingPlayer.setRoom(null);
        if (playerRepository.howManyUsersAreInTheRoom(targetRoom) == 0) {
            Long roomId = targetRoom.getId();
            targetRoom.getBoard().setRoom(null);
            targetRoom.setBoard(null);
            roomRepository.deleteRoom(roomId);
        }

        return Messages.SUCCESS_LEAVE_ROOM;
    }

    @Override
    public String resetRole(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        if (talkingPlayer.getTeam().equals("Red")) {
            if (talkingPlayer.getRole().equals("Spymaster")) {
                targetRoom.setRedSpyMaster(null);
            } else if (talkingPlayer.getRole().equals("Field Operator")) {
                targetRoom.setRedFieldOperator(null);
            }
        } else if (talkingPlayer.getTeam().equals("Blue")) {
            if (talkingPlayer.getRole().equals("Spymaster")) {
                targetRoom.setBlueSpyMaster(null);
            } else if (talkingPlayer.getRole().equals("Field Operator")) {
                targetRoom.setBlueFieldOperator(null);
            }
        }

        talkingPlayer.setRole(null);
        playerRepository.save(talkingPlayer);
        return Messages.SUCCESS_RESET;
    }

    @Override
    public String beSpymaster(String userId) {
        // TO-DO
        // NULL POINTER KALO GET NULL KEK GINI
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        try {
            if (talkingPlayer.getTeam().equals("Red")) {
                if (targetRoom.getRedSpyMaster() == null) {
                    if (talkingPlayer.getRole() == null) {
                        targetRoom.setRedSpyMaster(userId);
                        talkingPlayer.setRole("Spymaster");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    } else if (talkingPlayer.getRole().equals("Field Operator")) {
                        targetRoom.setRedFieldOperator(null);
                        targetRoom.setRedSpyMaster(userId);
                        talkingPlayer.setRole("Spymaster");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    }
                    responses = Messages.SUCCESS_BE_SPYMASTER;
                } else {
                    responses = Messages.FAIL_BE_SPYMASTER;
                }
            } else if (talkingPlayer.getTeam().equals("Blue")) {
                if (targetRoom.getBlueSpyMaster() == null) {
                    if (talkingPlayer.getRole() == null) {
                        targetRoom.setBlueSpyMaster(userId);
                        talkingPlayer.setRole("Spymaster");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    } else if (talkingPlayer.getRole().equals("Field Operator")) {
                        targetRoom.setBlueFieldOperator(null);
                        targetRoom.setBlueSpyMaster(userId);
                        talkingPlayer.setRole("Spymaster");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    }
                    responses = Messages.SUCCESS_BE_SPYMASTER;
                } else {
                    responses = Messages.FAIL_BE_SPYMASTER;
                }
            } else {
                responses = Messages.FAIL_BE_SPYMASTER;
            }
        } catch (NullPointerException e) {
            responses = Messages.FAIL_BE_SPYMASTER;
        }

        return this.responses;
    }

    @Override
    public String beFieldOperator(String userId) {
        // TO-DO
        // If Player haven't joined a team, FAIL
        // If Player have joined a team, but there's a Field Operator already, FAIL
        // If Player becomes a FO from being a SM
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        try {
            if (talkingPlayer.getTeam().equals("Red")) {
                if (targetRoom.getRedFieldOperator() == null) {
                    if (talkingPlayer.getRole() == null) {
                        targetRoom.setRedFieldOperator(userId);
                        talkingPlayer.setRole("Field Operator");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    } else if (talkingPlayer.getRole().equals("Spymaster")) {
                        targetRoom.setRedSpyMaster(null);
                        targetRoom.setRedFieldOperator(userId);
                        talkingPlayer.setRole("Field Operator");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    }
                    responses = Messages.SUCCESS_BE_FIELD_OPERATOR;
                } else {
                    responses = Messages.FAIL_BE_FIELD_OPERATOR;
                }
            } else if (talkingPlayer.getTeam().equals("Blue")) {
                if (targetRoom.getBlueFieldOperator() == null) {
                    if (talkingPlayer.getRole() == null) {
                        targetRoom.setBlueFieldOperator(userId);
                        talkingPlayer.setRole("Field Operator");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    } else if (talkingPlayer.getRole().equals("Spymaster")) {
                        targetRoom.setBlueSpyMaster(null);
                        targetRoom.setBlueFieldOperator(userId);
                        talkingPlayer.setRole("Field Operator");
                        playerRepository.save(talkingPlayer);
                        roomRepository.save(targetRoom);
                    }
                    responses = Messages.SUCCESS_BE_FIELD_OPERATOR;
                } else {
                    responses = Messages.FAIL_BE_FIELD_OPERATOR;
                }
            } else {
                responses = Messages.FAIL_BE_FIELD_OPERATOR;
            }
        } catch (NullPointerException e) {
            responses = Messages.FAIL_BE_FIELD_OPERATOR;
        }
        return this.responses;
    }

    @Override
    public String toRed(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        // If the amount of user with the aforementioned
        // roomId that has joined The Red Team = 2, FAIL
        if (playerRepository.howManyUsersJoinedThisTeamInThisRoom(targetRoom, "Red") == 2) {
            responses = Messages.FAIL_JOIN_RED;

        } else if (talkingPlayer.getRole() != null) {
            responses = Messages.CANT_CHANGE_TEAM_HAS_ROLE + talkingPlayer.getRole();
        } else {
            talkingPlayer.setTeam("Red");
            responses = Messages.SUCCESS_JOIN_RED;
            playerRepository.save(talkingPlayer);
        }
        return this.responses;
    }

    @Override
    public String toBlue(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        // If the amount of user with the aforementioned
        // roomId that has joined The Blue Team = 2, FAIL
        if (playerRepository.howManyUsersJoinedThisTeamInThisRoom(targetRoom, "Blue") == 2) {
            responses = Messages.FAIL_JOIN_BLUE;

        } else if (talkingPlayer.getRole() != null) {
            responses = Messages.CANT_CHANGE_TEAM_HAS_ROLE + talkingPlayer.getRole();
        } else {
            talkingPlayer.setTeam("Blue");
            responses = Messages.SUCCESS_JOIN_BLUE;
            playerRepository.save(talkingPlayer);
        }
        return this.responses;
    }

    @Override
    public String toBench(String userId) {
        // TO-DO
        // AGAIN, KALO DOI UDAH PUNYA ROLE REJECT
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        if (talkingPlayer.getRole() != null) {
            responses = Messages.CANT_CHANGE_TEAM_HAS_ROLE + talkingPlayer.getRole();
        } else {
            talkingPlayer.setTeam(null);
            responses = Messages.SUCCESS_MOVE_TO_BENCH;
            playerRepository.save(talkingPlayer);
        }
        return this.responses;
    }

    @Override
    public String start(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        String blueFOPlayer = targetRoom.getBlueFieldOperator();
        String blueSMPlayer = targetRoom.getBlueSpyMaster();
        String redFOPlayer = targetRoom.getRedFieldOperator();
        String redSMPlayer = targetRoom.getRedSpyMaster();
        String[] playersID = {blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer};


        //DEBUG
        //if(!isRoomComplete(playersID)){
        //targetRoom.setCurrentState(RedFieldOperatorState.DB_COL_NAME);
        //targetRoom.setChances(1);
        //targetRoom.setCurrentHint("TeStInG");
        //roomRepository.save(targetRoom);
        //return Messages.DEBUG_FO;
        //}

        if (isRoomComplete(playersID)) {
            String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                    Messages.SUCCESS_START, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);

            targetRoom.setCurrentState(RedSpymasterState.DB_COL_NAME);
            roomRepository.save(targetRoom);
            return messageWithPlayerID;
        }

        return Messages.FAIL_START;
    }

    @Override
    public String status(String userId) {
        // TO-DO
        responses = Messages.STATUS + "\n";
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();
        String roomIdInfo = String.valueOf(targetRoom.getId());
        responses += "Room ID: " + roomIdInfo + " \n";
        String team;
        String role;
        for (Player p : targetRoom.getPlayers()) {
            if (p.getTeam() == null) {
                team = "Bench";
            } else {
                team = p.getTeam();
            }

            if (p.getRole() == null) {
                role = "Unassigned Role";
            } else {
                role = p.getRole();
            }
            responses = responses + p.getDisplayName() + " - " + team + " - " + role + "\n";
        }
        return responses;
    }

    private boolean isRoomComplete(String[] players) {
        for (String p : players) {
            if (p == null) {
                return false;
            }
        }
        return true;
    }

    // In-game Commands
    @Override
    public String tell(String userId, String hint, int chances) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String peek(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String score(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String guess(String userId, int number) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String pass(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String hints(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String accept(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }

    @Override
    public String decline(String userId) {
        return Messages.FAIL_USING_GAME_COMMANDS;
    }
}
