package com.adprog9.botcodenames.handler.state.gamestate;

import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Board;
import com.adprog9.botcodenames.model.CardToBoard;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import org.springframework.stereotype.Component;

@Component
public class RedSpymasterState extends GameState {
    public static final String DB_COL_NAME = "RED_GUESSING";

    public RedSpymasterState() {
        this.responses = "";
    }

    //Pre-game Commands
    @Override
    public String help(String userId) {
        return Messages.HELP;
    }

    @Override
    public String createRoom(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String join(String userId, String roomId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String leave(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String resetRole(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String beSpymaster(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String beFieldOperator(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String toRed(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String toBlue(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String toBench(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String start(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    @Override
    public String status(String userId) {
        return Messages.FAIL_USING_NONGAME_COMMANDS;
    }

    public void emptyResponseList() {
        responses = "";
    }

    //In-game Commands
    @Override
    public String tell(String userId, String hint, int chances) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        if (talkingPlayer.getRole().equals("Spymaster") && talkingPlayer.getTeam().equals("Red")) {
            responses = String.format("%s\nHint: %s, Chances: %s\n%s",
                    Messages.TELL_RED, hint, chances,
                    Messages.RED_FIELD_OPERATOR_START);

            targetRoom.setCurrentHint(hint);
            targetRoom.setChances(chances);
            targetRoom.setCurrentState(RedFieldOperatorState.DB_COL_NAME);
            roomRepository.save(targetRoom);

            String blueFOPlayer = targetRoom.getBlueFieldOperator();
            String blueSMPlayer = targetRoom.getBlueSpyMaster();
            String redFOPlayer = targetRoom.getRedFieldOperator();
            String redSMPlayer = targetRoom.getRedSpyMaster();
            String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                    responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);


            return messageWithPlayerID;
        } else {
            responses = Messages.FAIL_UNAUTHORIZED + "Red Spymaster";
            return responses;
        }

    }

    @Override
    public String peek(String userId) {
        emptyResponseList();

        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        if (targetRoom.getCurrentHint() != null) {
            responses = String.format("Current hint: %s - Chances: %s \n",
                    targetRoom.getCurrentHint(),
                    String.valueOf(targetRoom.getChances()));
        } else {
            responses = "[DEBUG]";
        }

        Board board = targetRoom.getBoard();
        for (CardToBoard ctb : board.getCardToBoards()) {
            String number;
            String word;
            String role = "?";

            number = String.valueOf(ctb.getNumberInBoard());
            word = ctb.getCard().getWord();
            role = ctb.getRole();
            responses = responses + String.format("%s) %s - %s \n", number, word, role);

        }
        
        if(talkingPlayer.getRole().equals("Field Operator")){
            responses = Messages.FAIL_USING_SPYMASTER_COMMANDS;
        }
        return responses;
    }

    @Override
    public String score(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        int blueScore = targetRoom.getBlueScore();
        int redScore = targetRoom.getRedScore();
        responses = String.format("%s\nBlue: %s, Red: %s",
                Messages.ROOM_SCORE, blueScore, redScore);

        return responses;
    }

    @Override
    public String guess(String userId, int number) {
        return Messages.FAIL_USING_FIELDOPERATOR_COMMANDS;
    }

    @Override
    public String pass(String userId) {
        return Messages.FAIL_USING_FIELDOPERATOR_COMMANDS;
    }

    @Override
    public String hints(String userId) {
        return Messages.FAIL_USING_FIELDOPERATOR_COMMANDS;
    }

    @Override
    public String accept(String userId) {
        return Messages.FAIL_USING_FIELDOPERATOR_COMMANDS;
    }

    @Override
    public String decline(String userId) {
        return Messages.FAIL_USING_FIELDOPERATOR_COMMANDS;
    }
}
