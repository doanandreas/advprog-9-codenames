package com.adprog9.botcodenames.handler.state.gamestate;


import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Board;
import com.adprog9.botcodenames.model.CardToBoard;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import org.springframework.stereotype.Component;

@Component
public class RedFieldOperatorState extends GameState {
    public static final String DB_COL_NAME = "RED_FIELD_OPERATOR";

    public RedFieldOperatorState() {
        this.responses = "";
    }


    //Pre-game Commands
    @Override
    public String help(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String createRoom(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String join(String userId, String roomId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String leave(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String resetRole(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String beSpymaster(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String beFieldOperator(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String toRed(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String toBlue(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String toBench(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String start(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String status(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    public void emptyResponseList() {
        responses = "";
    }


    //In-game Commands
    @Override
    public String tell(String userId, String hint, int chances) {
        responses = Messages.FAIL_USING_SPYMASTER_COMMANDS;
        return responses;
    }

    @Override
    public String peek(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        if (targetRoom.getCurrentHint() != null) {
            responses = String.format("Current hint: %s - Chances: %s \n",
                    targetRoom.getCurrentHint(),
                    String.valueOf(targetRoom.getChances()));
        } else {
            responses = "[DEBUG]";
        }

      

        Board board = targetRoom.getBoard();
        for (CardToBoard ctb : board.getCardToBoards()) {
            String number;
            String word;
            String role = "?";

            number = String.valueOf(ctb.getNumberInBoard());
            word = ctb.getCard().getWord();
            if (ctb.getRevealed() == true) {
                role = ctb.getRole();
            }
            responses = responses + String.format("%s) %s - %s \n", number, word, role);

        }
        return responses;
    }

    @Override
    public String score(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String guess(String userId, int number) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        emptyResponseList();

        if (talkingPlayer.getRole().equals("Field Operator")
                && talkingPlayer.getTeam().equals("Red")) {
            int roomChances = targetRoom.getChances();
            int redScore = targetRoom.getRedScore();
            int blueScore = targetRoom.getBlueScore();
            String blueFOPlayer = targetRoom.getBlueFieldOperator();
            String blueSMPlayer = targetRoom.getBlueSpyMaster();
            String redFOPlayer = targetRoom.getRedFieldOperator();
            String redSMPlayer = targetRoom.getRedSpyMaster();


            targetRoom.setChances(roomChances - 1);
            CardToBoard guessedCard = targetRoom.getBoard().getCardToBoards().get(number - 1);
            for (CardToBoard ctb : targetRoom.getBoard().getCardToBoards()) {
                if (ctb.getNumberInBoard() == (number)) {
                    guessedCard = ctb;
                }
            }
            if (guessedCard.getRevealed() == true) {
                responses = Messages.CARD_ALREADY_GUESSED;
                return responses;
            } else {
                guessedCard.setRevealed(true);
                if (guessedCard.getRole().equals("Red Agent")) {
                    targetRoom.setRedScore(redScore + 1);
                    roomRepository.save(targetRoom);
                    responses = String.format("%s %s \n"
                            + "%s \n"
                            + "Red Score: %s \n"
                            + "Blue Score: %s",
                            Messages.RED_GUESS, number, Messages.ITS_RED,
                            redScore + 1, blueScore);

                    //If win, stop
                    if (redScore + 1 == 9) {
                        responses = responses + String.format("\n%s", Messages.RED_WINS_NORMAL);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
                        targetRoom.setCurrentState(RedWinState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        return messageWithPlayerID;
                    } else if (targetRoom.getChances() == 0) { //If eligible for One More
                        responses = responses + String.format("\n%s", Messages.RED_CAN_ONE_MORE);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
                        targetRoom.setCurrentState(RedOneMorePromptState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        return messageWithPlayerID;
                    } else {
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
                        return messageWithPlayerID;
                    }
                } else if (guessedCard.getRole().equals("Blue Agent")) {
                    targetRoom.setBlueScore(blueScore + 1);
                    roomRepository.save(targetRoom);
                    responses = String.format("%s %s \n"
                            + "%s \n"
                            + "Red Score: %s \n"
                            + "Blue Score: %s",
                            Messages.RED_GUESS, number, Messages.ITS_BLUE,
                            redScore, blueScore + 1);
                    //If the wrong answer made Blue Win
                    if (blueScore + 1 == 8) {
                        responses = responses + String.format("\n%s", Messages.BLUE_WINS_NORMAL);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
                        targetRoom.setCurrentState(BlueWinState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        return messageWithPlayerID;
                    } else { //Else the state shifts normally
                        responses = responses + String.format("\n%s",
                                Messages.BLUE_SPYMASTER_START);
                        targetRoom.setCurrentState(BlueSpymasterState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
                        return messageWithPlayerID;
                    }

                } else if (guessedCard.getRole().equals("ASSASSIN (!)")) {
                    //blue wins
                    responses = String.format("%s %s \n"
                            + "%s", Messages.RED_GUESS, number,
                            Messages.BLUE_WINS_ASSASSIN);
                    targetRoom.setCurrentState(BlueWinState.DB_COL_NAME);
                    roomRepository.save(targetRoom);
                    String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                            responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
                    return messageWithPlayerID;
                } else {
                    responses = String.format("%s %s \n"
                            + "%s \n"
                            + "Red Score: %s \n"
                            + "Blue Score: %s",
                            Messages.RED_GUESS, number, Messages.ITS_CIVILIAN,
                            redScore, blueScore);
                    responses = responses + String.format("\n%s", Messages.BLUE_SPYMASTER_START);
                    targetRoom.setCurrentState(BlueSpymasterState.DB_COL_NAME);
                    roomRepository.save(targetRoom);
                    String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                            responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
                    return messageWithPlayerID;
                }
            }

        } else {
            responses = Messages.FAIL_UNAUTHORIZED + "Red Field Operator";
            return responses;
        }
    }

    @Override
    public String pass(String userId) {
        emptyResponseList();
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        String blueFOPlayer = targetRoom.getBlueFieldOperator();
        String blueSMPlayer = targetRoom.getBlueSpyMaster();
        String redFOPlayer = targetRoom.getRedFieldOperator();
        String redSMPlayer = targetRoom.getRedSpyMaster();

        if (talkingPlayer.getRole().equals("Field Operator")
                && talkingPlayer.getTeam().equals("Red")) {
            targetRoom.setCurrentState(BlueSpymasterState.DB_COL_NAME);
            roomRepository.save(targetRoom);
            responses = Messages.RED_PASS + "\n" + Messages.BLUE_SPYMASTER_START;
            String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                    responses, blueFOPlayer, blueSMPlayer, redFOPlayer, redSMPlayer);
            return messageWithPlayerID;
        } else {
            responses = Messages.FAIL_UNAUTHORIZED + "Red Field Operator";
            return responses;
        }

    }

    @Override
    public String hints(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String accept(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String decline(String userId) {
        // TO-DO
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }
}
