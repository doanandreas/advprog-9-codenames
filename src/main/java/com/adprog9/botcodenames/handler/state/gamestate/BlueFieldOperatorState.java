package com.adprog9.botcodenames.handler.state.gamestate;


import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Board;
import com.adprog9.botcodenames.model.CardToBoard;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.model.Room;
import org.springframework.stereotype.Component;

@Component
public class BlueFieldOperatorState extends GameState {
    public static final String DB_COL_NAME = "BLUE_FIELD_OPERATOR";

    public BlueFieldOperatorState() {
        this.responses = "";
    }


    //Pre-game Commands
    @Override
    public String help(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String createRoom(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String join(String userId, String roomId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String leave(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String resetRole(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String beSpymaster(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String beFieldOperator(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String toBlue(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String toRed(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String toBench(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String start(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String status(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    public void emptyResponseList() {
        responses = "";
    }


    //In-game Commands
    @Override
    public String tell(String userId, String hint, int chances) {
        responses = Messages.FAIL_USING_SPYMASTER_COMMANDS;
        return responses;
    }

    @Override
    public String peek(String userId) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        if (targetRoom.getCurrentHint() != null) {
            responses = String.format("Current hint: %s - Chances: %s \n",
                    targetRoom.getCurrentHint(), String.valueOf(targetRoom.getChances()));
        } else {
            responses = "[DEBUG]";
        }

        Board board = targetRoom.getBoard();
        for (CardToBoard ctb : board.getCardToBoards()) {
            String number;
            String word;
            String role = "?";

            number = String.valueOf(ctb.getNumberInBoard());
            word = ctb.getCard().getWord();
            if (ctb.getRevealed() == true) {
                role = ctb.getRole();
            }
            responses = responses + String.format("%s) %s - %s \n",
                    number, word, role);

        }
        return responses;
    }

    @Override
    public String score(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String guess(String userId, int number) {
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        emptyResponseList();

        if (talkingPlayer.getRole().equals("Field Operator")
                && talkingPlayer.getTeam().equals("Blue")) {
            int roomChances = targetRoom.getChances();
            int blueScore = targetRoom.getBlueScore();
            int redScore = targetRoom.getRedScore();
            String redFOPlayer = targetRoom.getRedFieldOperator();
            String redSMPlayer = targetRoom.getRedSpyMaster();
            String blueFOPlayer = targetRoom.getBlueFieldOperator();
            String blueSMPlayer = targetRoom.getBlueSpyMaster();

            targetRoom.setChances(roomChances - 1);
            CardToBoard guessedCard = targetRoom.getBoard().getCardToBoards().get(number - 1);
            for (CardToBoard ctb : targetRoom.getBoard().getCardToBoards()) {
                if (ctb.getNumberInBoard() == (number)) {
                    guessedCard = ctb;
                }
            }
            if (guessedCard.getRevealed() == true) {
                responses = Messages.CARD_ALREADY_GUESSED;
                return responses;
            } else {
                guessedCard.setRevealed(true);
                if (guessedCard.getRole().equals("Blue Agent")) {
                    targetRoom.setBlueScore(blueScore + 1);
                    roomRepository.save(targetRoom);
                    responses = String.format("%s %s \n"
                            + "%s \n"
                            + "Blue Score: %s \n"
                            + "Red Score: %s",
                            Messages.BLUE_GUESS, number,
                            Messages.ITS_BLUE, blueScore + 1, redScore);

                    //If win, stop
                    if (blueScore + 1 == 9) {
                        responses = responses + String.format("\n%s", Messages.BLUE_WINS_NORMAL);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
                        targetRoom.setCurrentState(BlueWinState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        return messageWithPlayerID;
                    } else if (targetRoom.getChances() == 0) { //If eligible for One More
                        responses = responses + String.format("\n%s", Messages.BLUE_CAN_ONE_MORE);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
                        targetRoom.setCurrentState(BlueOneMorePromptState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        return messageWithPlayerID;
                    } else {
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
                        return messageWithPlayerID;
                    }
                } else if (guessedCard.getRole().equals("Red Agent")) {
                    targetRoom.setRedScore(redScore + 1);
                    roomRepository.save(targetRoom);
                    responses = String.format("%s %s \n"
                            + "%s \n"
                            + "Blue Score: %s \n"
                            + "Red Score: %s",
                            Messages.BLUE_GUESS, number, Messages.ITS_RED,
                            blueScore, redScore + 1);
                    //If the wrong answer made Red Win
                    if (redScore + 1 == 8) {
                        responses = responses + String.format("\n%s", Messages.RED_WINS_NORMAL);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
                        targetRoom.setCurrentState(RedWinState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        return messageWithPlayerID;
                    } else { //Else the state shifts normally
                        responses = responses + String.format("\n%s", Messages.RED_SPYMASTER_START);
                        targetRoom.setCurrentState(RedSpymasterState.DB_COL_NAME);
                        roomRepository.save(targetRoom);
                        String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                                responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
                        return messageWithPlayerID;
                    }

                } else if (guessedCard.getRole().equals("ASSASSIN (!)")) {
                    //red wins
                    responses = String.format("%s %s \n"
                            + "%s", Messages.BLUE_GUESS, number, Messages.RED_WINS_ASSASSIN);
                    targetRoom.setCurrentState(RedWinState.DB_COL_NAME);
                    roomRepository.save(targetRoom);
                    String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                            responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
                    return messageWithPlayerID;
                } else {
                    responses = String.format("%s %s \n"
                            + "%s \n"
                            + "Blue Score: %s \n"
                            + "Red Score: %s",
                            Messages.BLUE_GUESS, number,
                            Messages.ITS_CIVILIAN, blueScore, redScore);
                    responses = responses + String.format("\n%s", Messages.RED_SPYMASTER_START);
                    targetRoom.setCurrentState(RedSpymasterState.DB_COL_NAME);
                    roomRepository.save(targetRoom);
                    String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                            responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
                    return messageWithPlayerID;
                }
            }

        } else {
            responses = Messages.FAIL_UNAUTHORIZED + "Blue Field Operator";
            return responses;
        }
    }

    @Override
    public String pass(String userId) {
        emptyResponseList();
        Player talkingPlayer = playerRepository.findPlayerByUserId(userId);
        Room targetRoom = talkingPlayer.getRoom();

        String redFOPlayer = targetRoom.getRedFieldOperator();
        String redSMPlayer = targetRoom.getRedSpyMaster();
        String blueFOPlayer = targetRoom.getBlueFieldOperator();
        String blueSMPlayer = targetRoom.getBlueSpyMaster();

        if (talkingPlayer.getRole().equals("Field Operator")
                && talkingPlayer.getTeam().equals("Blue")) {
            targetRoom.setCurrentState(RedSpymasterState.DB_COL_NAME);
            roomRepository.save(targetRoom);
            responses = Messages.BLUE_PASS + "\n" + Messages.RED_SPYMASTER_START;
            String messageWithPlayerID = String.format("push;%s;%s %s %s %s",
                    responses, redFOPlayer, redSMPlayer, blueFOPlayer, blueSMPlayer);
            return messageWithPlayerID;
        } else {
            responses = Messages.FAIL_UNAUTHORIZED + "Blue Field Operator";
            return responses;
        }

    }

    @Override
    public String hints(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String accept(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }

    @Override
    public String decline(String userId) {
        emptyResponseList();
        responses = Messages.FAIL_USING_NONGAME_COMMANDS;
        return this.responses;
    }
}
