package com.adprog9.botcodenames.handler.helper;


import com.adprog9.botcodenames.handler.state.gamestate.*;
import com.adprog9.botcodenames.handler.state.pregamestate.GeneralState;
import com.adprog9.botcodenames.handler.state.pregamestate.LobbyState;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateHelper {
    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    private GeneralState generalState;

    @Autowired
    private LobbyState lobbyState;

    @Autowired
    private BlueWinState blueWinState;

    @Autowired
    private BlueFieldOperatorState blueFieldOperatorState;

    @Autowired
    private BlueSpymasterState blueSpymasterState;

    @Autowired
    private BlueOneMoreState blueOneMoreState;

    @Autowired
    private RedWinState redWinState;

    @Autowired
    private RedOneMoreState redOneMoreState;

    @Autowired
    private RedFieldOperatorState redFieldOperatorState;

    @Autowired
    private RedSpymasterState redSpymasterState;

    @Autowired
    private RedOneMorePromptState redOneMorePromptState;

    @Autowired
    private BlueOneMorePromptState blueOneMorePromptState;

    /**
     * Changing gamestate.
     */
    public GameState toState(String state) {
        switch (state) {
            case LobbyState.DB_COL_NAME:
                return lobbyState;
            case BlueWinState.DB_COL_NAME:
                return blueWinState;
            case BlueFieldOperatorState.DB_COL_NAME:
                return blueFieldOperatorState;
            case BlueSpymasterState.DB_COL_NAME:
                return blueSpymasterState;
            case BlueOneMoreState.DB_COL_NAME:
                return blueOneMoreState;
            case RedOneMorePromptState.DB_COL_NAME:
                return redOneMorePromptState;
            case RedWinState.DB_COL_NAME:
                return redWinState;
            case RedFieldOperatorState.DB_COL_NAME:
                return redFieldOperatorState;
            case RedSpymasterState.DB_COL_NAME:
                return redSpymasterState;
            case RedOneMoreState.DB_COL_NAME:
                return redOneMoreState;
            case BlueOneMorePromptState.DB_COL_NAME:
                return blueOneMorePromptState;
            default:
                return null;
        }
    }

    /**
     * Getter for user's state.
     */
    public GameState getUserState(String userID) {
        Player p = playerRepository.findPlayerByUserId(userID);
        if (p.getRoom() != null) {
            return toState(p.getRoom().getCurrentState());
        } else {
            return generalState;
        }
    }
}
