package com.adprog9.botcodenames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodenamesGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodenamesGameApplication.class, args);
    }

}
