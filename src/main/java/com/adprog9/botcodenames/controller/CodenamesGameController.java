package com.adprog9.botcodenames.controller;

import com.adprog9.botcodenames.controller.commands.CommandHandler;
import com.adprog9.botcodenames.controller.commands.InitialHandler;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Lazy;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@EnableDiscoveryClient
@EnableFeignClients
@RestController
public class CodenamesGameController implements GameController {

    @Autowired
    @Lazy
    private EurekaClient eurekaClient;

    @Autowired
    private MessageHandlerClient messageHandlerClient;

    @Autowired
    private CommandHandler commandHandler;

    public CodenamesGameController() {
        this.commandHandler = new InitialHandler();
    }

    @Value("${spring.application.name}")
    private String appName;

    @RequestMapping("/get-greeting")
    public String greeting(@RequestParam("name") String name, Model model) {
        return messageHandlerClient.greeting(name);
    }

    @Override
    public String gameGreeting(String clientAppName) {
        return String.format("Hello comrade %s! This is %s speaking from Controller, over.",
                clientAppName, eurekaClient.getApplication(appName).getName());
    }

    @Override
    public void respond(String message, String userID, String displayName, String replyToken) {
        String handlerResponse = commandHandler.handleCommand(message, userID, displayName);
        String responseType = handlerResponse.split(";")[0];

        if (responseType.equals("push")) {
            String responseAnswer = handlerResponse.split(";")[1];
            String usersToPush = handlerResponse.split(";")[2];
            messageHandlerClient.respondWithPush(responseAnswer, usersToPush);
        } else {
            messageHandlerClient.respondWithReply(handlerResponse, replyToken);
        }
    }
}
