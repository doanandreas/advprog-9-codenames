package com.adprog9.botcodenames.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface GameController {
    @RequestMapping("/playgame")
    String gameGreeting(@RequestParam("appname") String clientAppName);

    @RequestMapping("/respond-user")
    void respond(@RequestParam("message") String message,
                 @RequestParam("userid") String userId,
                 @RequestParam("displayname") String displayName,
                 @RequestParam("token") String replyToken);
}
