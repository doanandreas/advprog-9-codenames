package com.adprog9.botcodenames.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("message-handler-client")
public interface MessageHandlerClient {
    @RequestMapping("/greeting")
    String greeting(@RequestParam("name") String name);

    @RequestMapping("/replymsg")
    void respondWithReply(@RequestParam("response") String response,
                          @RequestParam("token") String replyToken);

    @RequestMapping("/pushmsg")
    void respondWithPush(@RequestParam("response") String response,
                         @RequestParam("user") String usersToSend);
}
