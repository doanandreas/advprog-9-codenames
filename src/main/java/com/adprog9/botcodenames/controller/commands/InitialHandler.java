package com.adprog9.botcodenames.controller.commands;

import com.adprog9.botcodenames.handler.helper.StateHelper;
import com.adprog9.botcodenames.handler.messages.Messages;
import com.adprog9.botcodenames.model.Player;
import com.adprog9.botcodenames.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class InitialHandler implements CommandHandler {
    /* urutan chain-nya sesuain sama urutan abjad folder ya, biar rapi aja :)
     * kl lu liat di sebelah (folder commands) urutannya chooseFO, chooseFM, createRoom, dll
     * exception cuma InitialHandler yang selalu ada di awal, dan
     * FailHandler, yang selalu di akhir chain.
     */

    @Autowired
    private StateHelper stateHelper;

    @Autowired
    private PlayerRepository playerRepository;

    //test
    String name;

    @Override
    public String handleCommand(String command, String userID, String displayName) {
        if (!playerRepository.isPlayerRegistered(userID)) {
            Player p = new Player(userID, displayName, null, null);
            playerRepository.save(p);
        }

        String[] splitCommand = command.split(" ");
        switch (splitCommand[0].toLowerCase()) {
            case ("/help"):
                return stateHelper.getUserState(userID).help(userID);
            case ("/createroom"):
                return stateHelper.getUserState(userID).createRoom(userID);
            case ("/join"):
                try {
                    return stateHelper.getUserState(userID).join(userID, splitCommand[1]);
                } catch (ArrayIndexOutOfBoundsException e) {
                    return Messages.UNRECOGNIZED_COMMAND;
                }
            case ("/leave"):
                return stateHelper.getUserState(userID).leave(userID);
            case ("/resetrole"):
                return stateHelper.getUserState(userID).resetRole(userID);
            case ("/bespymaster"):
                return stateHelper.getUserState(userID).beSpymaster(userID);
            case ("/befieldoperator"):
                return stateHelper.getUserState(userID).beFieldOperator(userID);
            case ("/tored"):
                return stateHelper.getUserState(userID).toRed(userID);
            case ("/toblue"):
                return stateHelper.getUserState(userID).toBlue(userID);
            case ("/tobench"):
                return stateHelper.getUserState(userID).toBench(userID);
            case ("/start"):
                return stateHelper.getUserState(userID).start(userID);
            case ("/status"):
                return stateHelper.getUserState(userID).status(userID);
            case ("/tell"):
                try {
                    String hint = splitCommand[1];
                    Integer chances = Integer.parseInt(splitCommand[2]);
                    return stateHelper.getUserState(userID).tell(userID, hint, chances);
                } catch (ArrayIndexOutOfBoundsException e) {
                    return Messages.UNRECOGNIZED_COMMAND;
                }
            case ("/peek"):
                return stateHelper.getUserState(userID).peek(userID);
            case ("/score"):
                return stateHelper.getUserState(userID).score(userID);
            case ("/guess"):
                try {
                    Integer numberGuess = Integer.parseInt(splitCommand[1]);
                    return stateHelper.getUserState(userID).guess(userID, numberGuess);
                } catch (ArrayIndexOutOfBoundsException e) {
                    return Messages.UNRECOGNIZED_COMMAND;
                }
            case ("/pass"):
                return stateHelper.getUserState(userID).pass(userID);
            case ("/hints"):
                return stateHelper.getUserState(userID).hints(userID);
            case ("/accept"):
                return stateHelper.getUserState(userID).accept(userID);
            case ("/decline"):
                return stateHelper.getUserState(userID).decline(userID);
            default:
                return Messages.UNRECOGNIZED_COMMAND;
        }
    }
}
