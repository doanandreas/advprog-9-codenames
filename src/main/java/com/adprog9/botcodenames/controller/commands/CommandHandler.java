package com.adprog9.botcodenames.controller.commands;


public interface CommandHandler {
    String handleCommand(String command, String userID, String displayName);
}
