package com.adprog9.botcodenames.generator;

import com.adprog9.botcodenames.model.Board;
import com.adprog9.botcodenames.model.Card;
import com.adprog9.botcodenames.model.CardToBoard;
import com.adprog9.botcodenames.model.Room;
import com.adprog9.botcodenames.repository.*;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class RoomGenerator {

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    CardToBoardRepository cardToBoardRepository;

    @Autowired
    BoardRepository boardRepository;

    @Autowired
    PlayerRepository playerRepository;

    List<Integer> boardNumList;

    /**
     * Generating Room.
     */
    public Room generateNewRoom() {
        Room room = new Room(0, 0, "LOBBY_STATE", "", 0);
        System.out.println("ROOM GENERATED HERE");
        Board board = new Board(room);
        List<Card> cards = cardRepository.findAll();
        Collections.shuffle(cards);

        //Make random agent places
        Integer[] boardNum = new Integer[25];
        for (int j = 0; j < 9; j++) {
            boardNum[j] = 2;
            System.out.println("A");
        }
        for (int j = 0; j < 8; j++) {
            boardNum[j + 9] = 1;
            System.out.println("B");
        }
        boardNum[17] = 3;
        for (int j = 0; j < 7; j++) {
            boardNum[j + 18] = 0;
            System.out.println("C");
        }

        for (int i = 0; i < 25; i++) {
            System.out.println(boardNum[i]);
        }
        boardNumList = Arrays.asList(boardNum);
        System.out.println("boardNumList checked");
        System.out.println(boardNumList);
        Collections.shuffle(boardNumList);

        for (int i = 0; i < 25; i++) {
            CardToBoard cardToBoard;
            if (boardNumList.get(i) == 3) {
                cardToBoard = new CardToBoard(i + 1, "ASSASSIN (!)", cards.get(i), board);
            } else if (boardNumList.get(i) == 1) {
                cardToBoard = new CardToBoard(i + 1, "Blue Agent", cards.get(i), board);
            } else if (boardNumList.get(i) == 2) {
                cardToBoard = new CardToBoard(i + 1, "Red Agent", cards.get(i), board);
            } else if (boardNumList.get(i) == 0) {
                cardToBoard = new CardToBoard(i + 1, "Innocent", cards.get(i), board);
            } else {
                cardToBoard = new CardToBoard(i, "Innocent", cards.get(i), board);
            }
            cardToBoardRepository.save(cardToBoard);
        }

        roomRepository.save(room);

        return room;
    }
}