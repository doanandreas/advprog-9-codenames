package com.adprog9.botcodenames.repository;

import com.adprog9.botcodenames.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PlayerRepository extends JpaRepository<Player, Long> {
    @Query(value = "SELECT * FROM PLAYER WHERE LINE_ID = ?1", nativeQuery = true)
    Player findPlayerByUserId(String lineId);

    @Query(value = "SELECT * FROM PLAYER WHERE DISPLAY_NAME = ?1", nativeQuery = true)
    Player findPlayerByDisplayName(String displayName);

    @Query(value = "SELECT * FROM PLAYER WHERE DISPLAY_NAME = ?1", nativeQuery = true)
    Player findPlayerLikeDisplayName(String displayName);

    @Query(value = "SELECT CASE WHEN COUNT(P) > 0 THEN true ELSE false END FROM Player P "
            + "WHERE P.LINE_ID = ?1",
            nativeQuery = true)
    boolean isPlayerRegistered(String userId);

    @Query(value = "SELECT STATE FROM PLAYER WHERE LINE_ID = ?1", nativeQuery = true)
    String getUserStateByID(String userId);

    @Query(value = "SELECT count(*) FROM Player where room_id = ?1", nativeQuery = true)
    int howManyUsersAreInTheRoom(Room room);

    @Query(value = "SELECT count(*) FROM PLAYER WHERE ROOM_ID = ?1 AND TEAM = ?2",
            nativeQuery = true)
    int howManyUsersJoinedThisTeamInThisRoom(Room room, String team);
}
