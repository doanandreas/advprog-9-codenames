package com.adprog9.botcodenames.repository;

import com.adprog9.botcodenames.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BoardRepository extends JpaRepository<Board, Long> {
    @Query(value = "SELECT room_id FROM BOARD WHERE board_id = ?1", nativeQuery = true)
    Board fetchBoardByRoomId(int boardId);
}
