package com.adprog9.botcodenames.repository;

import com.adprog9.botcodenames.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CardToBoardRepository extends JpaRepository<CardToBoard, Long> {

    @Query(value = "SELECT card_number FROM CARD_TO_BOARD WHERE board_id = ?1", nativeQuery = true)
    CardToBoard searchCardByBoardID(int boardId);

    @Query(value = "SELECT role FROM CARD_TO_BOARD WHERE board_id = ?1", nativeQuery = true)
    CardToBoard fetchRoleByBoardID(int boardId);

    @Query(value = "SELECT number_in_board FROM CARD_TO_BOARD WHERE board_id = ?1",
            nativeQuery = true)
    CardToBoard fetchNumberInBoardByBoardID(int boardId);
}
