package com.adprog9.botcodenames.repository;

import com.adprog9.botcodenames.model.*;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface RoomRepository extends JpaRepository<Room, Long> {
    @Query(value = "SELECT * FROM ROOM WHERE ID = ?1", nativeQuery = true)
    Room findRoomByIdRoom(Long roomId);

    @Query(value = "SELECT * FROM PLAYER WHERE ID_ROOM = ?1", nativeQuery = true)
    Player findPlayerLikeIdRoom(String roomId);

    @Modifying
    @Transactional
    @Query(value = "DELETE from ROOM WHERE ID = ?1", nativeQuery = true)
    void deleteRoom(Long roomId);
}