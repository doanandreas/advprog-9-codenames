package com.adprog9.botcodenames.repository;

import com.adprog9.botcodenames.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CardRepository extends JpaRepository<Card, Long> {
    @Query(value = "SELECT card_number FROM CARD WHERE card_number = ?1", nativeQuery = true)
    Card fetchCardByCardNumber(int cardNumber);
}