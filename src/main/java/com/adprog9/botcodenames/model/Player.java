package com.adprog9.botcodenames.model;

import javax.persistence.*;

@Entity
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "line_id")
    private String idLine;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "team")
    private String team;

    @Column(name = "role")
    private String role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id", nullable = true)
    private Room room;

    /**
     * Constructor.
     *
     * @param idLine id of player
     * @param displayName name of player
     * @param team team of player
     * @param role role of player
     */
    public Player(String idLine, String displayName, String team, String role) {
        this.idLine = idLine;
        this.displayName = displayName;
        this.team = team;
        this.role = role;
    }

    public Player() {
    }
    //public Player(String dummyRoom, String dummyID, String codename, String role, String s){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setIdLine(String idLine) {
        this.idLine = idLine;
    }

    public String getIdLine() {
        return idLine;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getTeam() {
        return team;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Room getRoom() {
        return room;
    }
}
