package com.adprog9.botcodenames.model;

import javax.persistence.*;

@Entity
public class CardToBoard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "card_id")
    private Card card;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "board_id")
    private Board board;

    @Column(name = "number_in_board")
    private int numberInBoard;

    @Column(name = "role")
    private String role;

    @Column(name = "revealed")
    private boolean revealed;

    public CardToBoard() {
    }

    /**
     * Constructor.
     *
     * @param numberInBoard number of this card on certain board
     * @param role role of the card
     * @param card object card
     * @param board board that has the card
     */
    public CardToBoard(int numberInBoard, String role, Card card, Board board) {
        this.numberInBoard = numberInBoard;
        this.role = role;
        this.revealed = false;
        this.card = card;
        this.card.getCardToBoards().add(this);
        this.board = board;
        this.board.getCardToBoards().add(this);
    }

    public void setRevealed(boolean revealed) {
        this.revealed = revealed;
    }

    public boolean getRevealed() {
        return revealed;
    }

    public void setCard(Card card) {
        this.card = card;
        this.card.getCardToBoards().add(this);
    }

    public Card getCard() {
        return card;
    }

    public void setNumberInBoard(int numberInBoard) {
        this.numberInBoard = numberInBoard;
    }

    public int getNumberInBoard() {
        return numberInBoard;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setBoard(Board board) {
        this.board = board;
        this.board.getCardToBoards().add(this);
    }

    public Board getBoard() {
        return board;
    }
}
