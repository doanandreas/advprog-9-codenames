package com.adprog9.botcodenames.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "board")
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    private Room room;

    @OneToMany(mappedBy = "board", cascade = CascadeType.ALL)
    private List<CardToBoard> cardToBoards;

    public Board() {
    }

    /**
     * Constructor.
     *
     * @param room room that has this board
     */
    public Board(Room room) {
        this.room = room;
        this.room.setBoard(this);
        this.cardToBoards = new ArrayList<>();
    }

    public List<CardToBoard> getCardToBoards() {
        return cardToBoards;
    }

    public void setCardToBoards(List<CardToBoard> cardToBoards) {
        this.cardToBoards = cardToBoards;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Room getRoom() {
        return room;
    }
}