package com.adprog9.botcodenames.model;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "card")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "word")
    private String word;

    @OneToMany(mappedBy = "card")
    private List<CardToBoard> cardToBoards;

    public Card(String word) {
        this.word = word;
        this.cardToBoards = new ArrayList<>();
    }

    public Card() {
    }

    public List<CardToBoard> getCardToBoards() {
        return cardToBoards;
    }

    public void setCardToBoards(List<CardToBoard> cardToBoards) {
        this.cardToBoards = cardToBoards;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

}
