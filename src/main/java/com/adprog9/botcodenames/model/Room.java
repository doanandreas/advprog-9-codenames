package com.adprog9.botcodenames.model;

import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "room")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "red_score")
    private int redScore;

    @Column(name = "blue_score")
    private int blueScore;

    @Column(name = "current_state")
    private String currentState;

    @Column(name = "red_field_operator")
    private String redFieldOperator;

    @Column(name = "red_spy_master")
    private String redSpyMaster;

    @Column(name = "blue_field_operator")
    private String blueFieldOperator;

    @Column(name = "blue_spy_master")
    private String blueSpyMaster;

    @Column(name = "hint")
    private String currentHint;

    @Column(name = "chances")
    private int chances;

    @OneToMany(mappedBy = "room", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private List<Player> players;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "room")
    @JoinColumn(name = "board_id", referencedColumnName = "id")
    private Board board;

    public Room() {
    }

    /**
     * Constructor.
     *
     * @param redScore red team score
     * @param blueScore blue team score
     * @param currentState current state
     * @param currentHint curremt hint
     * @param chances number of chance
     */
    public Room(int redScore, int blueScore,
                String currentState, String currentHint, int chances) {
        this.redScore = redScore;
        this.blueScore = blueScore;
        this.currentState = currentState;
        this.currentHint = currentHint;
        this.chances = chances;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRedScore(int redScore) {
        this.redScore = redScore;
    }

    public int getRedScore() {
        return redScore;
    }

    public void setBlueScore(int blueScore) {
        this.blueScore = blueScore;
    }

    public int getBlueScore() {
        return blueScore;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getCurrentState() {
        return currentState;
    }

    public String getRedFieldOperator() {
        return redFieldOperator;
    }

    public void setRedFieldOperator(String redFieldOperator) {
        this.redFieldOperator = redFieldOperator;
    }

    public String getRedSpyMaster() {
        return redSpyMaster;
    }

    public void setRedSpyMaster(String redSpyMaster) {
        this.redSpyMaster = redSpyMaster;
    }

    public String getBlueFieldOperator() {
        return blueFieldOperator;
    }

    public void setBlueFieldOperator(String blueFieldOperator) {
        this.blueFieldOperator = blueFieldOperator;
    }

    public String getBlueSpyMaster() {
        return blueSpyMaster;
    }

    public void setBlueSpyMaster(String blueSpyMaster) {
        this.blueSpyMaster = blueSpyMaster;
    }

    public void setBoard(Board board) {
        this.board = board;
        this.board.setRoom(this);
    }

    public List<Player> getPlayers() {
        return players;
    }

    /**
     * Set player of the room.
     */
    public void setPlayers(List<Player> players) {
        this.players = players;
        for (Player player : players) {
            player.setRoom(this);
        }
    }

    public Board getBoard() {
        return board;
    }

    public void setCurrentHint(String currentHint) {
        this.currentHint = currentHint;
    }

    public String getCurrentHint() {
        return currentHint;
    }

    public void setChances(int chances) {
        this.chances = chances;
    }

    public int getChances() {
        return chances;
    }
}
